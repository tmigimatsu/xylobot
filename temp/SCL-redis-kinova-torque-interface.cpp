#include <iostream>
#include <dlfcn.h> //Ubuntu
#include <KinovaTypes.h>
#include <Kinova.API.CommLayerUbuntu.h>
#include <Kinova.API.UsbCommandLayerUbuntu.h>
#include <unistd.h>
#include <time.h>

#include <hiredis/hiredis.h>
#include <signal.h>

#include <stdio.h>
#include <stdlib.h>
#include <vector>

using namespace std;

static volatile int exitCondition = 1;
TrajectoryPoint pointToSend;
AngularPosition data;

void intCtrlCHandler(int sig) {
  exitCondition = 0;
}

void sendState(redisReply *reply, redisContext *redis, string sensors_key) {
  reply = (redisReply *) redisCommand(redis, "SET %s_1 %f", sensors_key.c_str(), data.Actuators.Actuator1);
  freeReplyObject(reply);

  reply = (redisReply *) redisCommand(redis, "SET %s_2 %f", sensors_key.c_str(), data.Actuators.Actuator2);
  freeReplyObject(reply);

  reply = (redisReply *) redisCommand(redis, "SET %s_3 %f", sensors_key.c_str(), data.Actuators.Actuator3);
  freeReplyObject(reply);

  reply = (redisReply *) redisCommand(redis, "SET %s_4 %f", sensors_key.c_str(), data.Actuators.Actuator4);
  freeReplyObject(reply);

  reply = (redisReply *) redisCommand(redis, "SET %s_5 %f", sensors_key.c_str(), data.Actuators.Actuator5);
  freeReplyObject(reply);

  reply = (redisReply *) redisCommand(redis, "SET %s_6 %f", sensors_key.c_str(), data.Actuators.Actuator6);
  freeReplyObject(reply);
}

void setState(redisReply *reply, redisContext *redis, string sensors_key) {
  reply = (redisReply *) redisCommand(redis, "GET %s_1", sensors_key.c_str());
  pointToSend.Position.Actuators.Actuator1 = atof(reply->str);
  freeReplyObject(reply);

  reply = (redisReply *) redisCommand(redis, "GET %s_2", sensors_key.c_str());
  pointToSend.Position.Actuators.Actuator2 = atof(reply->str);
  freeReplyObject(reply);

  reply = (redisReply *) redisCommand(redis, "GET %s_3", sensors_key.c_str());
  pointToSend.Position.Actuators.Actuator3 = atof(reply->str);
  freeReplyObject(reply);

  reply = (redisReply *) redisCommand(redis, "GET %s_4", sensors_key.c_str());
  pointToSend.Position.Actuators.Actuator4 = atof(reply->str);
  freeReplyObject(reply);

  reply = (redisReply *) redisCommand(redis, "GET %s_5", sensors_key.c_str());
  pointToSend.Position.Actuators.Actuator5 = atof(reply->str);
  freeReplyObject(reply);

  reply = (redisReply *) redisCommand(redis, "GET %s_6", sensors_key.c_str());
  pointToSend.Position.Actuators.Actuator6 = atof(reply->str);
  freeReplyObject(reply);
}

int main()
{
  // Install Signal Handler
  signal(SIGINT, intCtrlCHandler);

  // Flag Variables
  int result;
  int programResult;
  int optionCode;

  /*
   * Option Code of 0:
   * Driver application writes joint angles to REDIS.
   *
   * Option Code of 1:
   * Driver application reads joint angles from REDIS
   * and updates the robot's position to them.
   *
   * Option Code of anything else:
   * Driver application both writes joint angles to REDIS
   * and reads joint torques from REDIS, sending them to
   * the robot. (Torques presumably computed from SCL).
   *
   */

  // Data Structures
  AngularPosition torque;
  AngularPosition torqueGravityFree;

  //Handle for the library's command layer.
  void * commandLayer_handle;

  //Function pointers to the functions we need
  int (*MyInitAPI)();
  int (*MyCloseAPI)();

  int (*MyGetAngularCommand)(AngularPosition &);
  int (*MyGetAngularPosition)(AngularPosition &);
  int (*MyGetAngularForce)(AngularPosition &Response);
  int (*MyGetAngularForceGravityFree)(AngularPosition &Response);
  int (*MyGetDevices)(KinovaDevice devices[MAX_KINOVA_DEVICE], int &result);
  int (*MySetActiveDevice)(KinovaDevice device);

  int (*MySendBasicTrajectory)(TrajectoryPoint command);
  int (*MySendAdvanceTrajectory)(TrajectoryPoint command);
  int (*MyEraseAllTrajectories)();

  int(*MySetGravityType)(GRAVITY_TYPE Type);
  int(*MySendAngularTorqueCommand)(float Command[COMMAND_SIZE]);
  int(*MyGetAngularTorqueCommand)(float Command[COMMAND_SIZE]);
  int(*MySetGravityOptimalZParam)(float Command[GRAVITY_PARAM_SIZE]);
  int(*MySetTorqueControlType)(TORQUECONTROL_TYPE type);
  int(*MySetTorqueVibrationController)(float value);
  int(*MySwitchTrajectoryTorque)(GENERALCONTROL_TYPE);
  int(*MySetTorqueSafetyFactor)(float factor);
  int(*MyMoveHome)();

  //We load the library
  commandLayer_handle = dlopen("Kinova.API.USBCommandLayerUbuntu.so",RTLD_NOW|RTLD_GLOBAL);

  //We load the functions from the library (Under Windows, use GetProcAddress)
  MyInitAPI = (int (*)()) dlsym(commandLayer_handle,"InitAPI");
  MyCloseAPI = (int (*)()) dlsym(commandLayer_handle,"CloseAPI");

  MyGetAngularCommand = (int (*)(AngularPosition &)) dlsym(commandLayer_handle,"GetAngularCommand");
  MyGetAngularPosition = (int (*)(AngularPosition &)) dlsym(commandLayer_handle,"GetAngularPosition");
  MyGetAngularForce = (int (*)(AngularPosition &Response)) dlsym(commandLayer_handle,"GetAngularForce");
  MyGetAngularForceGravityFree = (int (*)(AngularPosition &Response)) dlsym(commandLayer_handle,"GetAngularForceGravityFree");
  MyGetDevices = (int (*)(KinovaDevice devices[MAX_KINOVA_DEVICE], int &result)) dlsym(commandLayer_handle,"GetDevices");
  MySetActiveDevice = (int (*)(KinovaDevice devices)) dlsym(commandLayer_handle,"SetActiveDevice");

  MySendBasicTrajectory = (int (*)(TrajectoryPoint)) dlsym(commandLayer_handle,"SendBasicTrajectory");
  MySendAdvanceTrajectory = (int (*)(TrajectoryPoint)) dlsym(commandLayer_handle,"SendAdvanceTrajectory");
  MyEraseAllTrajectories = (int (*)()) dlsym(commandLayer_handle,"EraseAllTrajectories");

  MySetGravityType = (int(*)(GRAVITY_TYPE Type)) dlsym(commandLayer_handle, "SetGravityType");
  MySendAngularTorqueCommand = (int(*)(float Command[COMMAND_SIZE])) dlsym(commandLayer_handle, "SendAngularTorqueCommand");
  MyGetAngularTorqueCommand = (int(*)(float Command[COMMAND_SIZE])) dlsym(commandLayer_handle, "GetAngularTorqueCommand");
  MySetGravityOptimalZParam = (int(*)(float Command[GRAVITY_PARAM_SIZE])) dlsym(commandLayer_handle, "SetGravityOptimalZParam");
  MySetTorqueVibrationController = (int(*)(float)) dlsym(commandLayer_handle, "SetTorqueVibrationController");
  MySetTorqueControlType = (int(*)(TORQUECONTROL_TYPE)) dlsym(commandLayer_handle, "SetTorqueControlType");
  MySetTorqueSafetyFactor = (int(*)(float)) dlsym(commandLayer_handle, "SetTorqueSafetyFactor");
  MySwitchTrajectoryTorque = (int(*)(GENERALCONTROL_TYPE)) dlsym(commandLayer_handle, "SwitchTrajectoryTorque");
  MyMoveHome = (int(*)()) dlsym(commandLayer_handle, "MoveHome");

  //If the was loaded correctly
  if((MyInitAPI == NULL) || (MyCloseAPI == NULL) || (MyGetAngularCommand == NULL) || (MyGetAngularPosition == NULL)
      || (MySetActiveDevice == NULL) || (MyGetDevices == NULL))//|| (MyGetAngularForce == NULL) || (MyGetAngularForceGravityFree == NULL))
  {
    cout << "* * *  E R R O R   D U R I N G   I N I T I A L I Z A T I O N  * * *" << endl;
    programResult = 1;
  }
  else
  {
    cout << "I N I T I A L I Z A T I O N   C O M P L E T E D" << endl << endl;
    result = (*MyInitAPI)();
    int resultComm;
    AngularPosition DataCommand;
    // Get the angular command to test the communication with the robot
    resultComm = MyGetAngularCommand(DataCommand);
    cout << "Initialization's result :" << result << endl;
    cout << "Communication result :" << resultComm << endl;
    // If the API is initialized and the communication with the robot is working
    if (result == 1 && resultComm == 1)
    {
      cout << "API initialization worked" << flush << endl;

      // INITIALIZE REDIS

      string REDIS_HOST = "127.0.0.1";
      int REDIS_PORT = 6379;
      string sensors_key = "kinova:jointAngles";
      std::string torques_key = "kinova:jointTorques";
      redisContext *redis;
      redisReply *reply;

      reply = NULL;
      redis = redisConnect(REDIS_HOST.c_str(), REDIS_PORT);
      if (redis->err) {
        printf("Error: %s\n", redis->errstr);
      } else {
        printf("REDIS Connection Successful.\n");
      }

      reply = (redisReply *) redisCommand(redis, "GET OptionCode");
      optionCode = atoi(reply->str);
      freeReplyObject(reply);

      cout << "Read Option Code as: " << optionCode << flush << endl;

      // Set to position mode
      MySwitchTrajectoryTorque(POSITION);
      cout << "Set to Position Mode" << flush << endl;

      // Move to home position
      MyMoveHome();
      cout << "Moved to Home Position" << flush << endl;

      pointToSend.InitStruct();
      pointToSend.Position.Type = ANGULAR_POSITION;

      if(optionCode == 0)
      {
        while(exitCondition)
        {
          MyGetAngularPosition(data);
          sendState(reply, redis, sensors_key);
        }
      }
      else if(optionCode == 1)
      {
        while(exitCondition)
        {
          setState(reply, redis, sensors_key);
          MySendAdvanceTrajectory(pointToSend);
          usleep(50000);//Sleep for 50ms.

          /*
           * Other Scheme
           * Call "MyEraseAllTrajectories();" and then "MySendBasicTrajectory(pointToSend);"
           */
        }
      }
      else
      {
        // Set the Optimal parameters obtained from the identification sequence
        float OptimalParam[OPTIMAL_Z_PARAM_SIZE] = {1.1993, 0.0175869, -0.00114071, -1.14021, 0.00718503, 0.570303,
            0.00267999, 0.174534, -0.00615744, -0.00341871,0.505526, 0.132563, 0.0872487, 0.116211, 1.30409, 0.763368};
        // Set the gravity mode to Manual input
        MySetGravityOptimalZParam(OptimalParam);
        // Set gravity type to optimal
        MySetGravityType(OPTIMAL);

        cout << "Gravity Optimal Parameters Set" << flush << endl;

        // Set the torque control type to Direct Torque Control
        MySwitchTrajectoryTorque(TORQUE);
        MySetTorqueControlType(DIRECTTORQUE);

        cout << "Switch to Direct Torque Control" << flush << endl;

        // Set the safety factor off
        MySetTorqueSafetyFactor(1);
        // Set the vibration controller off
        MySetTorqueVibrationController(0);

        // Switch to torque control
        // Initialize the torque commands
        float TorqueCommand[COMMAND_SIZE];
        float Result[COMMAND_SIZE];
        float x;

        for (int i = 0; i < COMMAND_SIZE; i++)
        {
          TorqueCommand[i] = 0;
        }

        long long loopCounter = 0;
        while(exitCondition)
        {
          // Update Joint Angles in REDIS Key-Value Cache
          MyGetAngularPosition(data);
          sendState(reply, redis, sensors_key);

          // Fetch Torques
          for(int i = 0; i < 6; i++) {
            reply = (redisReply *) redisCommand(redis, "GET %s_%d", torques_key.c_str(), i);
            TorqueCommand[i] = -1*atof(reply->str);
            free(reply);
          }
          TorqueCommand[1] = -1*TorqueCommand[1];

          cout << "τ = (" << TorqueCommand[0] << ", " << TorqueCommand[1] << ", " << TorqueCommand[2] << ", ";
          cout << TorqueCommand[3] << ", " << TorqueCommand[4] << ", " << TorqueCommand[5] << ")" << endl;
          MySendAngularTorqueCommand(TorqueCommand);
          loopCounter++;
        }
      }

      MySwitchTrajectoryTorque(POSITION);
      MyMoveHome();
      programResult = 0;
    }
    else
    {
      cout << "API initialization failed" << endl;
      programResult = 1;
    }

    cout << endl << "C L O S I N G   A P I" << endl;
    result = (*MyCloseAPI)();
    programResult = 0;
  }

  dlclose(commandLayer_handle);
  return programResult;
}
