#include <iostream>
#include <dlfcn.h> //Ubuntu
#include "KinovaTypes.h"
#include "Kinova.API.CommLayerUbuntu.h"
#include "Kinova.API.UsbCommandLayerUbuntu.h"
#include <unistd.h>
#include <time.h>

#include <hiredis/hiredis.h>
#include <signal.h>

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <math.h>

using namespace std;

static volatile int g_exit_condition = 1;

static const int KINOVA_DOF = 6;
static const float KINOVA_ANGLE_OFFSET[] = {270.0, 180.0, 180.0, 270.0, 180.0, 90.0};
static const float KINOVA_ANGLE_SIGN[] = {-1.0, 1.0, -1.0, -1.0, -1.0, -1.0};
static const float RAD_PER_DEG = M_PI / 180.0;
static const float DEG_PER_RAD = 180.0 / M_PI;

// Set the Optimal parameters obtained from the identification sequence
// static float MY_GRAVITY[OPTIMAL_Z_PARAM_SIZE] = {1.1993, 0.0175869, -0.00114071, -1.14021, 0.00718503, 0.570303,
// 		0.00267999, 0.174534, -0.00615744, -0.00341871,0.505526, 0.132563, 0.0872487, 0.116211, 1.30409, 0.763368};
// static float MY_GRAVITY[OPTIMAL_Z_PARAM_SIZE] = {1.34249, 0.0118237, -0.000159366, -1.51963, 0.00946888, 0.830342,
// 		0.00328612, 0.281224, 0.00296992, -0.0108887, 0.611498, 0.198856, 0.160253, 0.15485, 1.29431, 0.81284};
// static float MY_GRAVITY[OPTIMAL_Z_PARAM_SIZE] = {1.34224, 0.0190583, 0.000873139, -1.53301, 0.0117792, 0.838829,
// 		0.00379095, 0.282384, 0.00613006, -0.011999, 0.590242, 0.113218, 0.157723, 0.153087, 1.00871, 0.530206};
// static float MY_GRAVITY[OPTIMAL_Z_PARAM_SIZE] = {1.34883, 0.0203581, -0.00026272, -1.54306, 0.0108026, 0.84677,
// 		0.00398146, 0.286589, 0.00554432, -0.0128132, 0.60873, 0.21131, 0.180164, 0.13832, 1.45497, 0.82277};
//
static float MY_GRAVITY[OPTIMAL_Z_PARAM_SIZE] = {1.30185, 0.0128249, 0.00232937, -1.41426, 0.00961193, 0.758498,
		0.0028024,0.252174, 0.000988963, 0.00193896, 0.67614, 0.315937, 0.228523, 0.159298, 1.46862, 0.864062};


static const char *REDIS_HOST = "127.0.0.1";
static const int REDIS_PORT = 6379;
static const char *REDIS_KEY_ANGLE = "kinova:jointAngles";
static const char *REDIS_KEY_VELOCITY = "kinova:jointVelocities";
static const char *REDIS_KEY_TORQUE = "kinova:jointTorques";
static const char *REDIS_KEY_CONTROL_MODE = "kinova:controlMode";
static char g_redis_buffer[1024];

typedef enum {REDIS_STATIC, REDIS_POSITION, REDIS_TORQUE, REDIS_FORCE} control_t;

void intCtrlCHandler(int sig) {
	if (g_exit_condition == 0) {
		exit(1);
	}
	g_exit_condition = 0;
}

// Read an array of 6 values from Redis
void getRedis(redisContext *redis, const char *key, float reply_val[KINOVA_DOF]) {
	redisReply *reply = (redisReply *) redisCommand(redis, "GET %s", key);
	sscanf(reply->str, "%f %f %f %f %f %f", &reply_val[0], &reply_val[1], &reply_val[2], &reply_val[3], &reply_val[4], &reply_val[5]);
	freeReplyObject(reply);
}

// Set an array of 6 values in Redis
void setRedis(redisContext *redis, const char *key, const float send_val[KINOVA_DOF]) {
	sprintf(g_redis_buffer, "%f %f %f %f %f %f", send_val[0], send_val[1], send_val[2], send_val[3], send_val[4], send_val[5]);
	redisReply *reply = (redisReply *) redisCommand(redis, "SET %s %s", key, g_redis_buffer);
	freeReplyObject(reply);
}

// Get SCL positions from Redis and convert to Kinova positions
void getRedisPosition(redisContext *redis, AngularPosition &kinova_position) {
	float q[KINOVA_DOF];

	// Get SCL radians from Redis and convert to Kinova degrees
	getRedis(redis, REDIS_KEY_ANGLE, q);
	for (int i = 0; i < KINOVA_DOF; i++) {
		q[i] = KINOVA_ANGLE_SIGN[i] * q[i] * DEG_PER_RAD + KINOVA_ANGLE_OFFSET[i];
	}

	kinova_position.Actuators.Actuator1 = q[0];
	kinova_position.Actuators.Actuator2 = q[1];
	kinova_position.Actuators.Actuator3 = q[2];
	kinova_position.Actuators.Actuator4 = q[3];
	kinova_position.Actuators.Actuator5 = q[4];
	kinova_position.Actuators.Actuator6 = q[5];
}

// Get SCL velocities from Redis and convert to Kinova velocities
void getRedisVelocity(redisContext *redis, AngularPosition &kinova_position) {
	float dq[KINOVA_DOF];

	// Get SCL radians from Redis and convert to Kinova degrees
	getRedis(redis, REDIS_KEY_VELOCITY, dq);
	for (int i = 0; i < KINOVA_DOF; i++) {
		dq[i] = KINOVA_ANGLE_SIGN[i] * dq[i] * DEG_PER_RAD;
	}

	kinova_position.Actuators.Actuator1 = dq[0];
	kinova_position.Actuators.Actuator2 = dq[1];
	kinova_position.Actuators.Actuator3 = dq[2];
	kinova_position.Actuators.Actuator4 = dq[3];
	kinova_position.Actuators.Actuator5 = dq[4];
	kinova_position.Actuators.Actuator6 = dq[5];
}

// Convert Kinova positions to SCL positions and set in Redis
void setRedisPosition(redisContext *redis, const AngularPosition &kinova_position) {
	float q[KINOVA_DOF];
	q[0] = kinova_position.Actuators.Actuator1;
	q[1] = kinova_position.Actuators.Actuator2;
	q[2] = kinova_position.Actuators.Actuator3;
	q[3] = kinova_position.Actuators.Actuator4;
	q[4] = kinova_position.Actuators.Actuator5;
	q[5] = kinova_position.Actuators.Actuator6;

	// Convert Kinova degrees to SCL radians and send to Redis
	for (int i = 0; i < KINOVA_DOF; i++) {
		q[i] = KINOVA_ANGLE_SIGN[i] * (q[i] - KINOVA_ANGLE_OFFSET[i]) * RAD_PER_DEG;
	}
	setRedis(redis, REDIS_KEY_ANGLE, q);
}

// Convert Kinova velocities to SCL velocities and set in Redis
void setRedisVelocity(redisContext *redis, const AngularPosition &kinova_position) {
	float dq[KINOVA_DOF];
	dq[0] = kinova_position.Actuators.Actuator1;
	dq[1] = kinova_position.Actuators.Actuator2;
	dq[2] = kinova_position.Actuators.Actuator3;
	dq[3] = kinova_position.Actuators.Actuator4;
	dq[4] = kinova_position.Actuators.Actuator5;
	dq[5] = kinova_position.Actuators.Actuator6;

	// Convert Kinova degrees to SCL radians and send to Redis
	for (int i = 0; i < KINOVA_DOF; i++) {
		dq[i] = KINOVA_ANGLE_SIGN[i] * (dq[i]) * RAD_PER_DEG;
	}
	setRedis(redis, REDIS_KEY_VELOCITY, dq);
}

// Get SCL torques from Redis and convert to Kinova torques
void getRedisTorque(redisContext *redis, float Gamma[KINOVA_DOF]) {
	// Get SCL torques from Redis and convert to Kinova torques
	getRedis(redis, REDIS_KEY_TORQUE, Gamma);
	for (int i = 0; i < KINOVA_DOF; i++) {
		Gamma[i] = KINOVA_ANGLE_SIGN[i] * Gamma[i];
	}
}

int main() {
	// Install Signal Handler
	signal(SIGINT, intCtrlCHandler);

	// Flag Variables
	int flag = false;
	control_t redis_control_mode;

	// Data Structures
	AngularPosition kinova_position;
	AngularPosition kinova_velocity;

	/*
	 * Option Code of 0:
	 * Driver application writes joint angles to REDIS.
	 *
	 * Option Code of 1:
	 * Driver application reads joint angles from REDIS
	 * and updates the robot's position to them.
	 *
	 * Option Code of 2:
	 * Driver application both writes joint angles to REDIS
	 * and reads joint torques from REDIS, sending them to
	 * the robot. (Torques presumably computed from SCL).
	 *
	 */

	//Handle for the library's command layer.
	void *commandLayer_handle;

	//Function pointers to the functions we need
	int (*MyInitAPI)();
	int (*MyCloseAPI)();

	int (*MyGetAngularCommand)(AngularPosition &);
	int (*MyGetAngularPosition)(AngularPosition &);
	int (*MyGetAngularVelocity)(AngularPosition &);
	int (*MyGetAngularForce)(AngularPosition &Response);
	int (*MyGetAngularForceGravityFree)(AngularPosition &Response);
	int (*MyGetDevices)(KinovaDevice devices[MAX_KINOVA_DEVICE], int &result);
	int (*MySetActiveDevice)(KinovaDevice device);

	int (*MySendBasicTrajectory)(TrajectoryPoint command);
	int (*MySendAdvanceTrajectory)(TrajectoryPoint command);
	int (*MyEraseAllTrajectories)();

	int(*MySetGravityType)(GRAVITY_TYPE Type);
	int(*MySendAngularTorqueCommand)(float Command[COMMAND_SIZE]);
	int(*MyGetAngularTorqueCommand)(float Command[COMMAND_SIZE]);
	int(*MySetGravityOptimalZParam)(float Command[GRAVITY_PARAM_SIZE]);
	int(*MySetGravityVector)(float Command[GRAVITY_VECTOR_SIZE]);
	int(*MySetTorqueControlType)(TORQUECONTROL_TYPE type);
	int(*MySetTorqueVibrationController)(float value);
	int(*MySwitchTrajectoryTorque)(GENERALCONTROL_TYPE);
	int(*MySetTorqueSafetyFactor)(float factor);
	int(*MyMoveHome)();

	/*** Load the Kinova API shared library ***/
	commandLayer_handle = dlopen("Kinova.API.USBCommandLayerUbuntu.so",RTLD_NOW|RTLD_GLOBAL);

	if (commandLayer_handle != NULL) {
		MyInitAPI = (int (*)()) dlsym(commandLayer_handle,"InitAPI");
		MyCloseAPI = (int (*)()) dlsym(commandLayer_handle,"CloseAPI");

		MyGetAngularCommand = (int (*)(AngularPosition &)) dlsym(commandLayer_handle,"GetAngularCommand");
		MyGetAngularPosition = (int (*)(AngularPosition &)) dlsym(commandLayer_handle,"GetAngularPosition");
		MyGetAngularVelocity = (int (*)(AngularPosition &)) dlsym(commandLayer_handle,"GetAngularVelocity");
		MyGetAngularForce = (int (*)(AngularPosition &Response)) dlsym(commandLayer_handle,"GetAngularForce");
		MyGetAngularForceGravityFree = (int (*)(AngularPosition &Response)) dlsym(commandLayer_handle,"GetAngularForceGravityFree");
		MyGetDevices = (int (*)(KinovaDevice devices[MAX_KINOVA_DEVICE], int &result)) dlsym(commandLayer_handle,"GetDevices");
		MySetActiveDevice = (int (*)(KinovaDevice devices)) dlsym(commandLayer_handle,"SetActiveDevice");

		MySendBasicTrajectory = (int (*)(TrajectoryPoint)) dlsym(commandLayer_handle,"SendBasicTrajectory");
		MySendAdvanceTrajectory = (int (*)(TrajectoryPoint)) dlsym(commandLayer_handle,"SendAdvanceTrajectory");
		MyEraseAllTrajectories = (int (*)()) dlsym(commandLayer_handle,"EraseAllTrajectories");

		MySetGravityType = (int(*)(GRAVITY_TYPE Type)) dlsym(commandLayer_handle, "SetGravityType");
		MySendAngularTorqueCommand = (int(*)(float Command[COMMAND_SIZE])) dlsym(commandLayer_handle, "SendAngularTorqueCommand");
		MyGetAngularTorqueCommand = (int(*)(float Command[COMMAND_SIZE])) dlsym(commandLayer_handle, "GetAngularTorqueCommand");
		MySetGravityOptimalZParam = (int(*)(float Command[GRAVITY_PARAM_SIZE])) dlsym(commandLayer_handle, "SetGravityOptimalZParam");
		MySetGravityVector = (int(*)(float Command[GRAVITY_VECTOR_SIZE])) dlsym(commandLayer_handle, "SetGravityVector");
		MySetTorqueVibrationController = (int(*)(float)) dlsym(commandLayer_handle, "SetTorqueVibrationController");
		MySetTorqueControlType = (int(*)(TORQUECONTROL_TYPE)) dlsym(commandLayer_handle, "SetTorqueControlType");
		MySetTorqueSafetyFactor = (int(*)(float)) dlsym(commandLayer_handle, "SetTorqueSafetyFactor");
		MySwitchTrajectoryTorque = (int(*)(GENERALCONTROL_TYPE)) dlsym(commandLayer_handle, "SwitchTrajectoryTorque");
		MyMoveHome = (int(*)()) dlsym(commandLayer_handle, "MoveHome");
	}

	// Check that the library was loaded correctly
	char *err = dlerror();
	if (err != NULL) {
		cout << endl << "* * *  E R R O R   L O A D I N G   L I B R A R Y  * * *" << endl;
		cout << err << endl;
		if (commandLayer_handle != NULL) {
			dlclose(commandLayer_handle);
			commandLayer_handle = NULL;
		}
	}

	// Initialize API
	if (commandLayer_handle != NULL && !(*MyInitAPI)()) {
		cout << "* * *  E R R O R   I N I T I A L I Z I N G   A P I  * * *" << endl << endl;
		dlclose(commandLayer_handle);
		commandLayer_handle = NULL;
	}

	// Test communication with robot
	if (commandLayer_handle != NULL && !MyGetAngularCommand(kinova_position)) {
		cout << endl << "* * *  E R R O R   C O M M U N I C A T I N G   W I T H   R O B O T * * *" << endl;
		cout << "C L O S I N G   A P I" << endl;
		(*MyCloseAPI)();
		dlclose(commandLayer_handle);
		commandLayer_handle = NULL;
	}

	if (commandLayer_handle != NULL) {
		cout << endl << "A P I   I N I T I A L I Z A T I O N   C O M P L E T E D" << endl;
	} else {
		// Set dummy lambda functions in place of API functions
		MyInitAPI = []() { return 0; };
		MyCloseAPI = []() { return 0; };

		MyGetAngularCommand = [](AngularPosition &ap) { return 0; };
		MyGetAngularPosition = [](AngularPosition &ap) { return 0; };
		MyGetAngularVelocity = [](AngularPosition &ap) { return 0; };
		MyGetAngularForce = [](AngularPosition &ap) { return 0; };
		MyGetAngularForceGravityFree = [](AngularPosition &ap) { return 0; };
		MyGetDevices = [](KinovaDevice kd[MAX_KINOVA_DEVICE], int &i) { return 0; };
		MySetActiveDevice = [](KinovaDevice kd) { return 0; };

		MySendBasicTrajectory = [](TrajectoryPoint tp) { return 0; };
		MySendAdvanceTrajectory = [](TrajectoryPoint tp) { return 0; };
		MyEraseAllTrajectories = []() { return 0; };

		MySetGravityType = [](GRAVITY_TYPE gt) { return 0; };
		MySendAngularTorqueCommand = [](float f[COMMAND_SIZE]) { return 0; };
		MyGetAngularTorqueCommand = [](float f[COMMAND_SIZE]) { return 0; };
		MySetGravityOptimalZParam = [](float f[GRAVITY_PARAM_SIZE]) { return 0; };
		MySetGravityVector = [](float f[GRAVITY_VECTOR_SIZE]) { return 0; };
		MySetTorqueVibrationController = [](float) { return 0; };
		MySetTorqueControlType = [](TORQUECONTROL_TYPE tc) { return 0; };
		MySetTorqueSafetyFactor = [](float) { return 0; };
		MySwitchTrajectoryTorque = [](GENERALCONTROL_TYPE gc) { return 0; };
		MyMoveHome = []() { return 0; };

		cout << endl << "C O N T I N U I N G   W I T H O U T   A P I" << endl;
	}

	/*** Initialize Redis ***/

	redisContext *redis = redisConnect(REDIS_HOST, REDIS_PORT);

	if (redis->err) {
		cout << endl << "* * *  E R R O R   C O M M U N I C A T I N G   W I T H   R E D I S * * *" << endl;
		cout << redis->errstr << endl;
		cout << "C L O S I N G   A P I" << endl;
		(*MyCloseAPI)();
		if (commandLayer_handle != NULL) {
			dlclose(commandLayer_handle);
		}
		return 1;
	}

	/*** Initialize robot ***/

	MySwitchTrajectoryTorque(POSITION);
	MyMoveHome();
	cout << "Set to position mode and moved to home position" << endl;

	/*** Get control mode from Redis ***/
	//TODO: Surround everything in try/catch
	redisReply *reply = (redisReply *) redisCommand(redis, "GET %s", REDIS_KEY_CONTROL_MODE);
	if (reply->type == REDIS_REPLY_NIL) {
		cout << "Cannot get control mode from Redis. Missing key: " << REDIS_KEY_CONTROL_MODE << endl;
		cout << "C L O S I N G   A P I" << endl;
		(*MyCloseAPI)();
		if (commandLayer_handle != NULL) {
			dlclose(commandLayer_handle);
		}
		return 1;
	};
	redis_control_mode = (control_t) atoi(reply->str);
	freeReplyObject(reply);

	cout << "Read control mode as: " << redis_control_mode << endl;

	switch (redis_control_mode) {
		case REDIS_STATIC: {
			cout << endl << "U S I N G   S T A T I C   M O D E" << endl;
			while (g_exit_condition) {
				MyGetAngularPosition(kinova_position);
				setRedisPosition(redis, kinova_position);
			}
			break;
		}

		case REDIS_POSITION: {
			cout << endl << "U S I N G   P O S I T I O N   C O N T R O L" << endl;
			TrajectoryPoint traj_point;
			traj_point.InitStruct();
			traj_point.Position.Type = ANGULAR_POSITION;

			while (g_exit_condition) {
				getRedisPosition(redis, kinova_position);
				traj_point.Position.Actuators = kinova_position.Actuators;
				MySendAdvanceTrajectory(traj_point);
				usleep(50000);//Sleep for 50ms.
			}
			break;
		}

		case REDIS_TORQUE: {
			cout << endl << "U S I N G   T O R Q U E   C O N T R O L" << endl;
			// Set the gravity mode to manual input and gravity type to optimal
			MySetGravityOptimalZParam(MY_GRAVITY);
			MySetGravityType(OPTIMAL);
			// MySetGravityVector({0.0, 0.0, 0.0});

			// Set the torque control type to Direct Torque Control
			MySwitchTrajectoryTorque(TORQUE);
			MySetTorqueControlType(DIRECTTORQUE);

			// Set the safety factor on
			MySetTorqueSafetyFactor(1);
			// Set the vibration controller on
			MySetTorqueVibrationController(1);

			// Initialize the torque commands
			float torque_command[COMMAND_SIZE];
			for (int i = 0; i < COMMAND_SIZE; i++) {
				torque_command[i] = 0;
			}
			setRedis(redis, REDIS_KEY_TORQUE, torque_command);
			MySendAngularTorqueCommand(torque_command);

			long long loop_counter = 0;
			while (g_exit_condition) {
				// Send joint angles to Redis
				MyGetAngularPosition(kinova_position);
				setRedisPosition(redis, kinova_position);
				// Send the joint velocities to Redis
				MyGetAngularVelocity(kinova_velocity);
				setRedisVelocity(redis, kinova_velocity);

				if (loop_counter % 3 == 0) {
					// Get joint torques from Redis
					getRedisTorque(redis, torque_command);

					// Set all torques to 0 if any are nan
					for (int i = 0; i < KINOVA_DOF; i++) {
						if (isnan(torque_command[i])) {
							for (int j = 0; j < KINOVA_DOF; j++) {
								torque_command[i] = 0;
							}
							break;
						}
					}

					MySendAngularTorqueCommand(torque_command);
				}

				// Print torques
				if (loop_counter % 100 == 0) {
					cout << "Torque : ";
					for (int i = 0; i < KINOVA_DOF; i++) {
						cout << torque_command[i] << " ";
					}
					cout << endl;
				}

				// usleep(10000);//Sleep for 10ms.
				// usleep(1000000);//Sleep for 0.1ms.
				loop_counter++;
			}
			break;
		}

		default:
			break;
	}

	/*** Reset robot ***/

	// Set to position mode and move to home position
	MySwitchTrajectoryTorque(POSITION);
	MyMoveHome();
	cout << endl << "Set to position mode and moved to home position" << endl;

	// Close the API
	cout << endl << "C L O S I N G   A P I" << endl;
	(*MyCloseAPI)();

	if (commandLayer_handle != NULL) {
		dlclose(commandLayer_handle);
	}
	return 0;
}
