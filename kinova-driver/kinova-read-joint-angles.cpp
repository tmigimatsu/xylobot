#include <iostream>
#include <dlfcn.h>
#include <vector>
#include "Kinova.API.CommLayerUbuntu.h"
#include "KinovaTypes.h"
#include <stdio.h>

using namespace std;

int main()
{
  int result;
  AngularPosition dataCommand;
  AngularPosition dataPosition;
  AngularPosition torque;
  AngularPosition torqueGravityFree;

  //Handle for the library's command layer.
  void * commandLayer_handle;

  //Function pointers to the functions we need
  int (*MyInitAPI)();
  int (*MyCloseAPI)();
  int (*MyGetAngularCommand)(AngularPosition &);
  int (*MyGetAngularPosition)(AngularPosition &);
  int (*MyGetAngularForce)(AngularPosition &Response);
  int (*MyGetAngularForceGravityFree)(AngularPosition &Response);
  int (*MyGetDevices)(KinovaDevice devices[MAX_KINOVA_DEVICE], int &result);
  int (*MySetActiveDevice)(KinovaDevice device);

  //We load the library
  commandLayer_handle = dlopen("Kinova.API.USBCommandLayerUbuntu.so",RTLD_NOW|RTLD_GLOBAL);

  //We load the functions from the library (Under Windows, use GetProcAddress)
  MyInitAPI = (int (*)()) dlsym(commandLayer_handle,"InitAPI");
  MyCloseAPI = (int (*)()) dlsym(commandLayer_handle,"CloseAPI");
  MyGetAngularCommand = (int (*)(AngularPosition &)) dlsym(commandLayer_handle,"GetAngularCommand");
  MyGetAngularPosition = (int (*)(AngularPosition &)) dlsym(commandLayer_handle,"GetAngularPosition");
  MyGetAngularForce = (int (*)(AngularPosition &Response)) dlsym(commandLayer_handle,"GetAngularForce");
  MyGetAngularForceGravityFree = (int (*)(AngularPosition &Response)) dlsym(commandLayer_handle,"GetAngularForceGravityFree");
  MyGetDevices = (int (*)(KinovaDevice devices[MAX_KINOVA_DEVICE], int &result)) dlsym(commandLayer_handle,"GetDevices");
  MySetActiveDevice = (int (*)(KinovaDevice devices)) dlsym(commandLayer_handle,"SetActiveDevice");

  //If the was loaded correctly
  if((MyInitAPI == NULL) || (MyCloseAPI == NULL) || (MyGetAngularCommand == NULL) || (MyGetAngularPosition == NULL)
     || (MySetActiveDevice == NULL) || (MyGetDevices == NULL))//|| (MyGetAngularForce == NULL) || (MyGetAngularForceGravityFree == NULL))
  {
    cout << "* * *  E R R O R   D U R I N G   I N I T I A L I Z A T I O N  * * *" << endl;
  }
  else
  {
    cout << "I N I T I A L I Z A T I O N   C O M P L E T E D" << endl << endl;

    result = (*MyInitAPI)();

    cout << "Initialization's result :" << result << endl;

    KinovaDevice list[MAX_KINOVA_DEVICE];

    int devicesCount = MyGetDevices(list, result);

    for(long ii=0;ii<10000;ii++){
      for(int i = 0; i < devicesCount; i++)
      {
        cout << "Found a robot on the USB bus (" << list[i].SerialNumber << ") (" << list[i].DeviceType << ")" << endl;

        //Setting the current device as the active device.
        MySetActiveDevice(list[i]);

        (*MyGetAngularCommand)(dataCommand);
        (*MyGetAngularPosition)(dataPosition);
        cout << "*********************************" << endl;
        cout << "Actuator 1   command : " << dataCommand.Actuators.Actuator1 << "°" << "     Position : " << dataPosition.Actuators.Actuator1 <<  endl;
        cout << "Actuator 2   command : " << dataCommand.Actuators.Actuator2 << "°" << "     Position : " << dataPosition.Actuators.Actuator2 <<  endl;
        cout << "Actuator 3   command : " << dataCommand.Actuators.Actuator3 << "°" << "     Position : " << dataPosition.Actuators.Actuator3 <<  endl;
        cout << "Actuator 4   command : " << dataCommand.Actuators.Actuator4 << "°" << "     Position : " << dataPosition.Actuators.Actuator4 <<  endl;
        cout << "Actuator 5   command : " << dataCommand.Actuators.Actuator5 << "°" << "     Position : " << dataPosition.Actuators.Actuator5 <<  endl;
        cout << "Actuator 6   command : " << dataCommand.Actuators.Actuator6 << "°" << "     Position : " << dataPosition.Actuators.Actuator6 <<  endl << endl;
        cout << "  Finger 1   command : " << dataCommand.Fingers.Finger1 << "     Position : " << dataPosition.Fingers.Finger1 <<  endl;
        cout << "  Finger 2   command : " << dataCommand.Fingers.Finger2 << "     Position : " << dataPosition.Fingers.Finger2 <<  endl;
        cout << "  Finger 3   command : " << dataCommand.Fingers.Finger3 << "     Position : " << dataPosition.Fingers.Finger3 <<  endl;
        cout << "*********************************" << endl;/*
        MyGetAngularForce(torque);
        MyGetAngularForceGravityFree(torqueGravityFree);
        cout << "Actuator 1   torque : " << torque.Actuators.Actuator1 << " N*m" << "     without gravity : " << torqueGravityFree.Actuators.Actuator1 << " N*m" <<  endl;
        cout << "Actuator 2   torque : " << torque.Actuators.Actuator2 << " N*m" << "     without gravity : " << torqueGravityFree.Actuators.Actuator2 << " N*m"  <<  endl;
        cout << "Actuator 3   torque : " << torque.Actuators.Actuator3 << " N*m" << "     without gravity : " << torqueGravityFree.Actuators.Actuator3 << " N*m"  <<  endl;
        cout << "Actuator 4   torque : " << torque.Actuators.Actuator4 << " N*m" << "     without gravity : " << torqueGravityFree.Actuators.Actuator4 << " N*m"  <<  endl;
        cout << "Actuator 5   torque : " << torque.Actuators.Actuator5 << " N*m" << "     without gravity : " << torqueGravityFree.Actuators.Actuator5 << " N*m"  <<  endl;
        cout << "Actuator 6   torque : " << torque.Actuators.Actuator6 << " N*m" << "     without gravity : " << torqueGravityFree.Actuators.Actuator6 << " N*m"  <<  endl;
        cout << "*********************************" << endl << endl << endl;*/
      }
    }

    cout << endl << "C L O S I N G   A P I" << endl;
    result = (*MyCloseAPI)();
  }

  dlclose(commandLayer_handle);

  return 0;
}
