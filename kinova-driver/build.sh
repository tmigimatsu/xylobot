#!/bin/bash
g++ kinova-read-joint-angles.cpp -ldl -o kinova-read-joint-angles
g++ kinova-gc-ctrl.cpp -ldl -o kinova-gc-ctrl
g++ kinova-joint-ctrl.cpp -ldl -o kinova-joint-ctrl
g++ kinova-torque-ctrl.cpp -ldl -o kinova-torque-ctrl
g++ Minimal_Torque_Control_Example.cpp -ldl -o kinova-minimal-torque
g++ -std=c++11 SCL-redis-kinova-torque-interface.cpp -ldl -o kinova-driver -l hiredis
g++ ZEstimation.cpp -ldl -o ZEstimation
