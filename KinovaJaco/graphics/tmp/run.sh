#!/bin/bash
for i in `ls *STL | cut -f 1 -d ' '`; do
	echo Converting : $i
	./meshconv $i -c obj -o `echo $i | cut -f 1 -d '.'`
done
