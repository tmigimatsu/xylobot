/* This file is part of scl, a control and simulation library
for robots and biomechanical models.

scl is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

Alternatively, you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

scl is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License and a copy of the GNU General Public License along with
scl. If not, see <http://www.gnu.org/licenses/>.
*/
/* \file XylobotCtrl.hpp
 *
 *  Created on: May 15, 2011
 *
 *  Copyright (C) 2011
 *
 *  Author: Samir Menon <smenon@stanford.edu>
 */
/*
 * Setup steps for a standard 1-robot simulation.
 */

#ifndef XYLOBOT_CTRL_HPP_
#define XYLOBOT_CTRL_HPP_

//Standard includes
#include <scl/DataTypes.hpp>
#include <scl/Singletons.hpp>
#include <scl/robot/DbRegisterFunctions.hpp>
#include <scl/parser/sclparser/CParserScl.hpp>
#include <scl/dynamics/tao/CDynamicsTao.hpp>
#include <scl/dynamics/scl/CDynamicsScl.hpp>
#include <scl_ext/dynamics/scl_spatial/CDynamicsSclSpatial.hpp>
#include <scl/control/task/tasks/CTaskOpPos.hpp>
#include <scl/graphics/chai/CGraphicsChai.hpp>
#include <scl/graphics/chai/ChaiGlutHandlers.hpp>
#include <sutil/CSystemClock.hpp>
#include <scl/util/DatabaseUtils.hpp>
#include <scl/util/HelperFunctions.hpp>
#include <scl/data_structs/SRigidBody.hpp>
#include "chai3d.h"

#include <omp.h>
#include <GL/freeglut.h>
#include <Eigen/Dense>
#include <hiredis/hiredis.h>

#include <sstream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <queue>
#include <cmath>
#include <csignal>

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

static const int KINOVA_DOF = 6;
static const char *ARDUINO_DEV = "/dev/ttyUSB0";

namespace scl_app {

	/* Generic code required to run a scl simulation
	 * A simulation requires running 3 things. This example uses:
	 * 1. A dynamics/physics engine                  :  Tao
	 * 2. A controller                               :  Scl
	 * 3. A graphic rendering+interaction interface  :  Chai3d + FreeGlut
	 */
class SHiredisStruct{
	public:
		redisContext *context = nullptr;
		redisReply *reply = nullptr;
		bool initialized = false;
		char buffer[1024];
		const char *hostname = "127.0.0.1";
		const int port = 6379;
		const timeval timeout = {1, 500000}; // 1.5 seconds
		const char *key_positions = "kinova:jointAngles";
		const char *key_velocities = "kinova:jointVelocities";
		const char *key_torques = "kinova:jointTorques";
		const char *key_control_mode = "kinova:controlMode";
		const char *key_notes = "music_notes";
		const char *key_times = "music_times";
};

static void signalHandler(int sig) {
	redisContext *context = redisConnectWithTimeout("127.0.0.1", 6379, {0, 250000});
	redisCommand(context, "SET %s %s", "kinova:jointTorques", "0.0 0.0 0.0 0.0 0.0 0.0");
	exit(1);
}


enum KinovaControlMode { STATIC, POSITION, TORQUE, FORCE, SIMULATION };

enum XylophoneStatus { INITIALIZING, QUIET, PLAYING, STRIKING };

enum ControlType { EQUAL_PRIORITY, POSITION_PRIORITY, ORIENTATION_PRIORITY };

class XylobotCtrl {

	public:

		/* Specs parameters */
		std::string specs_file, robot_name, controller_name;

		std::vector<std::string> task_names;
		std::map<std::string, scl::STaskOpPos *> tasks_ds;

		Eigen::Vector3d x_des, x_err, dx_err, d_phi, w_err;
		Eigen::Matrix3d R_des;
		Eigen::VectorXd Gamma, Gamma_kinova;

		SHiredisStruct redis;
		KinovaControlMode control_mode;
		XylophoneStatus music_status;
		std::queue<int> music_notes;
		std::queue<double> music_times;
		Eigen::Vector3d pos_xylophone;
		std::vector<Eigen::Vector3d> pos_keys;
		std::vector<chai3d::cGenericObject *> xylophone_keys;
		chai3d::cGenericObject *goal_sphere;
		int fd_arduino;

		double t_start, t_curr, t_goal, t_goal_interval, t_wait;
		int note_goal, note_goal_prev;
		chai3d::cColorf color_key;
		// const double color_key[3] = {0.3, 0.3, 0.35};
		const double color_key_pressed[3] = {0.0, 1.0, 0.0};
		const double color_key_missed[3] = {1.0, 0.0, 0.0};
		const double time_mallet = 0.5;

		void communicateRedisMusic();
		void communicateRedisReceiveKinova();
		void communicateRedisSendKinova();
		void computeControlForces(enum ControlType);
		inline bool endEffectorIsStable();

		// Read an array of 6 values from Redis
		void getRedisArray(const char *key, float reply_val[KINOVA_DOF]) {
			redis.reply = (redisReply *) redisCommand(redis.context, "GET %s", key);
			if (redis.reply->type == REDIS_REPLY_NIL) throw std::runtime_error("Could not get " + std::string(key));
			sscanf(redis.reply->str, "%f %f %f %f %f %f", &reply_val[0], &reply_val[1], &reply_val[2], &reply_val[3], &reply_val[4], &reply_val[5]);
			freeReplyObject(redis.reply);
		}

		// Set an array of 6 values in Redis
		void setRedisArray(const char *key, const float send_val[KINOVA_DOF]) {
			sprintf(redis.buffer, "%f %f %f %f %f %f", send_val[0], send_val[1], send_val[2], send_val[3], send_val[4], send_val[5]);
			redis.reply = (redisReply *) redisCommand(redis.context, "SET %s %s", key, redis.buffer);
			if (redis.reply->type == REDIS_REPLY_NIL) throw std::runtime_error("Could not set " + std::string(key));
			freeReplyObject(redis.reply);
		}

		/* Implement this function. Else you will get a linker error. */
		inline void stepMySimulation();

		/* Implement this function. Else you will get a linker error. */
		bool initMyController(int argc, char** argv);

		/* Default constructor. Sets stuff to zero. */
		XylobotCtrl()
			: specs_file("KinovaJaco/KinovaJacoCfg.xml")
			, robot_name("KinovaJacoBot")
			, controller_name("opc")
			, task_names({"hand_p", "hand_r"})
			, control_mode(STATIC)
			, music_status(INITIALIZING)
			, fd_arduino(-1)
			, t_start(0.0)
			, t_curr(0.0)
			, t_goal(0.0)
			, t_goal_interval(0.0)
			, t_wait(0.0)
			, note_goal(0)
			, note_goal_prev(0)

 			, db(nullptr)
			, robot_ds(nullptr)
			, robot_io(nullptr)
			, iter_controller(0)
			, iter_graphics(0) {}

		/* Destructor: Cleans up */
		~XylobotCtrl() {}

		/************************************************************************/
		/****************NOTE : You should NOT need to modify this **************/
		/****************       But feel free to use the objects   **************/
		/************************************************************************/

		/** Initialize the basic global variables, database etc.*/
		void init(int argc, char** argv);

		/** Terminates the simulation and prints some statistics */
		void terminate(int status);

		/** Runs a simulation using two threads:
		 * Thread 1: Computes the robot dynamics
		 * Thread 2: Renders the graphics and handles gui interaction */
		void runMainLoopThreaded();

		/** Runs a simulation using one thread.
		 * 1: Computes the robot dynamics
		 * 2: Renders the graphics and handles gui interaction */
		void runMainLoop();

		scl::SDatabase *db;                 //Generic database (for sharing data)
		scl::SRobotParsed *robot_ds;          //Generic robot ds
		scl::SRobotIO *robot_io;       //Access the robot's sensors and actuators
		scl::SGcModel robot_gcm;          //Robot data structure with dynamic quantities

		scl_ext::CDynamicsSclSpatial dyn_tao;          //Generic tao dynamics
		scl::CDynamicsScl dyn_scl;          //Generic tao dynamics
		scl::CGraphicsChai chai;         //Generic chai graphics
		scl::SGraphicsChai *chai_ds;

		std::vector<std::string> robots_parsed;   //Parsed robots
		std::vector<std::string> graphics_parsed; //Parsed graphics views

		long long iter_controller;            //Controller computation counter
		long long iter_graphics;              //Controller computation counter

		int getXylophoneKeyIdx(int pitch) {
			switch (pitch) {
				// White keys
				case 55: return 0;
							case 56: return 16;
				case 57: return 1;
							case 58: return 17;
				case 59: return 2;

				case 60: return 3;
							case 61: return 18;
				case 62: return 4;
							case 63: return 19;
				case 64: return 5;
				case 65: return 6;
							case 66: return 20;
				case 67: return 7;
							case 68: return 21;
				case 69: return 8;
							case 70: return 22;
				case 71: return 9;

				case 72: return 10;
							case 73: return 23;
				case 74: return 11;
							case 75: return 24;
				case 76: return 12;
				case 77: return 13;
							case 78: return 25;
				case 79: return 14;
							case 80: return 26;
				case 81: return 15;

				default: return -1;
			}
		}

};


/************************************************************************/
/****************NOTE : You should NOT need to modify this **************/
/****************       All the function implementations   **************/
/************************************************************************/

void XylobotCtrl::init(int argc, char** argv) {
	// Parse optional command line arguments
	if (argc >= 2) specs_file = argv[1];
	if (argc >= 3) robot_name = argv[2];
	if (argc >= 4) controller_name = argv[3];
	if (argc >= 5) {
		task_names.clear();
		for (int i = 4; i < argc; i++)
			task_names.push_back(argv[i]);
	}

	try {
		/******************** Initialization ********************/
		if (!sutil::CSystemClock::start())
			throw std::runtime_error("Could not start clock");

		db = scl::CDatabase::getData();
		if (db == nullptr)
			throw std::runtime_error("Database not initialized");

		// Set the specs dir so scl knows where the graphics are.
		db->dir_specs_ = db->cwd_; // + std::string("../../specs/");

		// For parsing controllers
		if (!scl_registry::registerNativeDynamicTypes())
			throw std::runtime_error("Could not register native dynamic types");

		std::cout << "Initialized clock and database. Start Time:" << sutil::CSystemClock::getSysTime() << std::endl;

		/******************** Parse Files ********************/
		std::cout << "Running scl task controller for input file: " << specs_file << std::endl;

		scl::CParserScl xml_parser;
		bool flag = scl_registry::parseEverythingInFile(specs_file, &xml_parser, &robots_parsed, &graphics_parsed);
		if (!flag || robots_parsed.size() <= 0 || graphics_parsed.size() <= 0)
			throw std::runtime_error("Could not parse the file");

		if (!scl_util::isStringInVector(robot_name, robots_parsed))
			throw std::runtime_error("Could not find passed robot name in file");

		/******************** Shared I/O Data Structure ********************/
		robot_ds = db->s_parser_.robots_.at(robot_name);
		if (robot_ds == nullptr)
			throw std::runtime_error("Robot data structure does not exist in the database");

		robot_io = db->s_io_.io_data_.at(robot_name);
		if (robot_io == nullptr)
			throw std::runtime_error("Robot I/O data structure does not exist in the database");

		/******************** Redis Database ********************/

		redis.context = redisConnectWithTimeout(redis.hostname, redis.port, redis.timeout);
		if (redis.context == NULL) {
			std::cout << "Could not allocate redis context. Continuing without redis." << std::endl;
		} else if (redis.context->err) {
			std::cout << "Could not connect to redis server. Continuing without redis : " << redis.context->errstr << std::endl;
			redisFree(redis.context);
		} else {
			// PING server to make sure things are working..
			redis.reply = (redisReply *)redisCommand(redis.context, "PING");
			std::cout << "SCL Redis Task : Pinged Redis server. Reply is, " << redis.reply->str << std::endl;
			freeReplyObject(redis.reply);

			std::cout << "** To monitor Redis messages, open a redis-cli and type 'monitor' **" << std::endl;
			redis.initialized = true;
		}

		/******************** Dynamics ********************/
		if (!robot_gcm.init(*robot_ds))
			throw std::runtime_error("Could not initialize robot's gc model");

		if (!dyn_tao.init(*robot_ds))
			throw std::runtime_error("Could not initialize physics simulator");

		if (!dyn_scl.init(*robot_ds))
			throw std::runtime_error("Could not initialize dynamics algorithms");

		/******************** ChaiGlut Graphics ********************/
		if (!db->s_gui_.glut_initialized_) {
			glutInit(&argc, argv);
			db->s_gui_.glut_initialized_ = true;
		}

		scl::SGraphicsParsed *gr_parsed = db->s_parser_.graphics_worlds_.at(graphics_parsed[0]);
		chai_ds = db->s_gui_.chai_data_.at(graphics_parsed[0]);
		if (!chai.initGraphics(gr_parsed, chai_ds))
			throw std::runtime_error("Couldn't initialize chai graphics");

		if (!chai.addRobotToRender(robot_ds, robot_io))
			throw std::runtime_error("Couldn't add robot to the chai rendering object");

		if (!scl_chai_glut_interface::initializeGlutForChai(graphics_parsed[0], &chai))
			throw std::runtime_error("Glut initialization error");

		/******************** Arduino ********************/
		fd_arduino = open(ARDUINO_DEV, O_RDWR | O_NONBLOCK);
		if (fd_arduino == -1) std::cout << "fd_arduino = -1\n";
		fcntl(fd_arduino, F_SETFL, 0);
		struct termios options;
		if (tcgetattr(fd_arduino, &options) < 0) std::cout << "could not get fd_arduino options" << std::endl;
		cfsetispeed(&options, B9600);
	  cfsetospeed(&options, B9600);
    options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8;
		options.c_cflag &= ~CRTSCTS;
		options.c_cflag |= CREAD | CLOCAL;
		options.c_cc[VMIN] = 0;
		options.c_cc[VTIME] = 0;
		tcsetattr(fd_arduino, TCSANOW, &options);
		if (tcsetattr(fd_arduino, TCSAFLUSH, &options) < 0) std::cout << "could not set arduino options" << std::endl;

		/******************** Initialize User Controller ********************/
		if (!initMyController(argc, argv))
			throw std::runtime_error("Could not initialize user's custom controller");

		//Simulation loop.
		std::cout << "Starting simulation. Integration timestep: " << db->sim_dt_ << std::endl << std::flush;

	} catch(std::exception & e) {
		std::cout << "XylobotCtrl::setup() Failed : " << e.what() << std::endl;
		terminate(1);
	}
}

void XylobotCtrl::terminate(int status) {
	/******************************Termination************************************/
	if (!chai.destroyGraphics())
		std::cout << "Error deallocating graphics pointers"; //Sanity check.
	redis.reply = (redisReply *) redisCommand(redis.context, "SET %s %s", redis.key_torques, "0 0 0 0 0 0");
	freeReplyObject(redis.reply);
	if (fd_arduino >= 0) close(fd_arduino);

	/****************************Print Collected Statistics*****************************/
	std::cout << "Total Simulated Time : " << sutil::CSystemClock::getSimTime() << " sec" << std::endl
	          << "Total Control Model and Servo Updates : " << iter_controller << std::endl
	          << "Total Graphics Updates                : " << iter_graphics << std::endl
	          << "End Time:" << sutil::CSystemClock::getSysTime() << std::endl
	          << "********************************************" << std::endl << std::flush;
	exit(status);
}

void XylobotCtrl::runMainLoopThreaded() {
	omp_set_num_threads(2);
	int thread_id;

#pragma omp parallel private(thread_id)
	{//Start threaded region

		thread_id = omp_get_thread_num();
		if (thread_id == 1) {
			//Thread 1 : Run the simulation
			while (scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running) {
				if (!scl::CDatabase::getData()->pause_ctrl_dyn_) {
    				t_curr = sutil::CSystemClock::getSysTime();
					stepMySimulation();

					sutil::CSystemClock::tick(db->sim_dt_);
					iter_controller++; //Increment the counter for dynamics computed.
					const timespec ts = {0, 5000};/*.05ms*/
					//const timespec ts = {0, 10000};/*0.1 ms*/
					nanosleep(&ts,NULL);
				} else if (scl::CDatabase::getData()->step_ctrl_dyn_) {
					//If paused, but step required, step it and set step flag to false.
					scl::CDatabase::getData()->step_ctrl_dyn_ = false;
					stepMySimulation();
				} else {
					//Paused and no step required. Sleep for a bit.
					const timespec ts = {0, 15000000};//Sleep for 15ms
					nanosleep(&ts, NULL);
				}
			}
		} else {
			//Thread 2 : Run the graphics and gui
			while (scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running) {
				const timespec ts = {0, 15000000};//Sleep for 15ms
				nanosleep(&ts, NULL);
				glutMainLoopEvent(); //Update the graphics
				iter_graphics++;
			}
		}
	}//End of threaded region
}

void XylobotCtrl::runMainLoop() {
	while (scl_chai_glut_interface::CChaiGlobals::getData()->chai_glut_running) {
		if (!scl::CDatabase::getData()->pause_ctrl_dyn_) {
			//If not paused, step the simulation.
			stepMySimulation();
		} else if(scl::CDatabase::getData()->step_ctrl_dyn_) {
			//If paused, but step required, step it and set step flag to false.
			scl::CDatabase::getData()->step_ctrl_dyn_ = false;
			stepMySimulation();
		}

		static int iter_skip_graphics = 0;
		if (iter_skip_graphics <= 500) {
			iter_skip_graphics++;
			continue;
		}
		iter_skip_graphics = 0;
		glutMainLoopEvent(); //Update the graphics
		iter_graphics++;
	}
}

}
#endif /* CSCL_APP_TASK_HPP_ */
