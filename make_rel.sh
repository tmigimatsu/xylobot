mkdir -p build_rel &&
cd build_rel &&
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON &&
make -j8 &&
cp -rf xylobot_ctrl ../ &&
cd ..
