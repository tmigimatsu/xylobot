import math
import numpy
import scipy.io.wavfile as wavfile
from scipy import signal
import sys
import media_util
import analyzer

def addToFeatureMatrix(matrix, feature):
	feature = feature.reshape(1, feature.shape[0])
	if matrix.size is 0:
		matrix = feature;
	else:
		matrix = numpy.vstack((matrix, feature))
	return matrix

def saveFile(filename, data):
	numpy.savetxt(filename, data, delimiter=',', fmt='%.2f', newline = '\n')

def extractFeaturesOnsets(filename, onsets):
	rate, data = wavfile.read(filename)
	if data.ndim > 1:
		data = media_util.stereoToMono(data)
	len_seconds = len(data) / rate
	matrix = numpy.array([])
	# numNotes = 7
	# samplesPerNote = data.size / numNotes
	# for i in range(0, numNotes):
	# 	data_seg = data[i * samplesPerNote : (i + 1) * samplesPerNote]
	# 	features = analyzer.getFrequencies(data_seg)[0:5000]
	# 	# append the label
	# 	# features = numpy.append(features, 55 + i)
	# 	# add to the matrix
	# 	if i == 30:
	# 		analyzer.plotSample(features)
	# 	matrix = addToFeatureMatrix(matrix, features)
	# saveFile(filename + '_seg.csv', matrix)
	for i in range(0, len(onsets) - 1):
		note_start = onsets[i]
		note_end = onsets[i + 1]
		data_seg = data[note_start : note_end]
		features = analyzer.getFrequencies(data_seg)[0:5000]
		matrix = addToFeatureMatrix(matrix, features)
	data_seg = data[onsets[len(onsets) - 1] : -1]
	features = analyzer.getFrequencies(data_seg)[0:5000]
	matrix = addToFeatureMatrix(matrix, features)
	return matrix


def extractFeatures(filename):
	rate, data = wavfile.read(filename)
	if data.ndim > 1:
		data = media_util.stereoToMono(data)
	len_seconds = len(data) / rate
	matrix = numpy.array([])
	numNotes = 7
	samplesPerNote = data.size / numNotes
	for i in range(0, numNotes):
		data_seg = data[i * samplesPerNote : (i + 1) * samplesPerNote]
		features = analyzer.getFrequencies(data_seg)[0:5000]
		# append the label
		# features = numpy.append(features, 55 + i)
		# add to the matrix
		if i == 30:
			analyzer.plotSample(features)
		matrix = addToFeatureMatrix(matrix, features)
	saveFile(filename + '_seg.csv', matrix)

if __name__ == '__main__':
	track = sys.argv[1]
	extractFeatures(track)
