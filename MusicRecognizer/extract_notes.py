#! /usr/bin/env python

import sys, math, redis
from aubio import onset, source, pitch
from numpy import hstack, zeros, argmax

if len(sys.argv) < 2:
	print "Usage: %s <filename> [samplerate]" % sys.argv[0]
	sys.exit(1)

filename = sys.argv[1]
r = redis.Redis(host='171.64.70.79');

downsample = 2
samplerate = 48000 / downsample
if len(sys.argv) > 2: samplerate = int(sys.argv[2])

win_s = 4096 / downsample # fft size
hop_s = 512  / downsample # hop size

s = source(filename, samplerate, hop_s)
samplerate = s.samplerate

o = onset("default", win_s, hop_s, samplerate)

# list of onsets, in samples
onsets = []
onsets_seconds = []

tolerance = 0.8

pitch_o = pitch("yin", win_s, hop_s, samplerate)
pitch_o.set_unit("midi")
pitch_o.set_tolerance(tolerance)

pitches = []
confidences = []

# storage for plotted data
desc = []
tdesc = []
allsamples_max = zeros(0,)
#downsample = 1  # to plot n samples / hop_s

# pitch detection stuff
conf_thresh = 0.8
searching_for_p = False
last_highest = 0.0
pitch_bins = zeros(200)

# total number of frames read
total_frames = 0
while True:
	samples, read = s()
	if o(samples):
		print "PITCH:", argmax(pitch_bins[0:90])
		pitches.append(argmax(pitch_bins[0:90]))
		print "%f" % (o.get_last_s())
		searching_for_p = True
		onsets.append(o.get_last())
		onsets_seconds.append(o.get_last_s())
		pitch_bins = zeros(200)
	pitch = pitch_o(samples)[0]
	#pitch = int(round(pitch))
	lo_pitch = int(math.floor(pitch))
	hi_pitch = int(math.ceil(pitch))
	hi_weight = pitch % 1.0
	confidence = pitch_o.get_confidence()
	if lo_pitch > 0:
		#pitch_bins[pitch] += confidence
		pitch_bins[lo_pitch] += confidence * (1 - hi_weight)
		pitch_bins[hi_pitch] += confidence * hi_weight


	# keep some data to plot it later
	new_maxes = (abs(samples.reshape(hop_s/downsample, downsample))).max(axis=0)
	allsamples_max = hstack([allsamples_max, new_maxes])
	desc.append(o.get_descriptor())
	tdesc.append(o.get_thresholded_descriptor())
	total_frames += read
	if read < hop_s: break
print "PITCH:", argmax(pitch_bins)
pitches.append(argmax(pitch_bins))
pitches.pop(0)
print pitches
print onsets_seconds
print dir(o)

# while r.lpop('music_notes'):
# 	print 'removed a note'
#
# while r.lpop('music_times'):
# 	print 'removed an onset'
#
# for i in range(0, len(pitches)):
# 	r.lpush('music_notes', pitches[i])
# 	r.lpush('music_times', onsets_seconds[i])

if 1:
	# do plotting
	import matplotlib.pyplot as plt
	allsamples_max = (allsamples_max > 0) * allsamples_max
	allsamples_max_times = [ float(t) * hop_s / downsample / samplerate for t in range(len(allsamples_max)) ]
	plt1 = plt.axes([0.1, 0.75, 0.8, 0.19])
	plt2 = plt.axes([0.1, 0.1, 0.8, 0.65], sharex = plt1)
	plt.rc('lines',linewidth='.8')
	plt1.plot(allsamples_max_times,  allsamples_max, '-b')
	plt1.plot(allsamples_max_times, -allsamples_max, '-b')
	for stamp in onsets:
		stamp /= float(samplerate)
		plt1.plot([stamp, stamp], [-1., 1.], '-r')
	plt1.axis(xmin = 0., xmax = max(allsamples_max_times) )
	plt1.xaxis.set_visible(False)
	plt1.yaxis.set_visible(False)
	desc_times = [ float(t) * hop_s / samplerate for t in range(len(desc)) ]
	desc_plot = [d / max(desc) for d in desc]
	plt2.plot(desc_times, desc_plot, '-g')
	tdesc_plot = [d / max(desc) for d in tdesc]
	for stamp in onsets:
		stamp /= float(samplerate)
		plt2.plot([stamp, stamp], [min(tdesc_plot), max(desc_plot)], '-r')
	plt2.plot(desc_times, tdesc_plot, '-y')
	plt2.axis(ymin = min(tdesc_plot), ymax = max(desc_plot))
	plt.xlabel('time (s)')
	#plt.savefig('/tmp/t.png', dpi=200)
	plt.show()
