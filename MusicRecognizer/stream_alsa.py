import alsaaudio
import time
import sys
import termios
import tty
import select
from wav_bandpass import bandpass_filter
from plot_onset import detect_notes
import numpy as np
import struct
import wave

card_index = 2
sample_rate = 48000

# Open the device in nonblocking capture mode. The last argument could
# just as well have been zero for blocking mode. Then we could have
# left out the sleep call in the bottom of the loop
print('Opening sound card.')
#inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NONBLOCK, cardindex=card_index)
inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, cardindex=card_index)

#  # Set attributes: Mono, 48000 Hz, 16 bit little endian samples
inp.setchannels(1)
inp.setrate(sample_rate)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)

# The period size controls the internal number of frames per period.
# The significance of this parameter is documented in the ALSA api.
# For our purposes, it is suficcient to know that reads from the device
# will return this many frames. Each frame being 2 bytes long.
# This means that the reads below will return either 320 bytes of data
# or 0 bytes of data. The latter is possible because we are in nonblocking
# mode.
inp.setperiodsize(2048)

filename = 'stream'

old_settings = termios.tcgetattr(sys.stdin)

class StreamState:
    WAITING    = 0
    INITIALIZING = 1
    RECORDING  = 2
    PROCESSING = 3

state = StreamState.WAITING
try:
    tty.setcbreak(sys.stdin.fileno())
    data_np = []
    print('\nReady. Press <space> to record.\n')
    while True:

        # Non-blocking io to get character
        if select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], []):
            c = sys.stdin.read(1)
            if state == StreamState.WAITING and c == ' ':
                state = StreamState.INITIALIZING
                print('Initializing recording.')
            elif state == StreamState.RECORDING and c == ' ':
                state = StreamState.PROCESSING
                print('Processing.')

        if state == StreamState.INITIALIZING:
            # Non-blocking io to read from sound card
            length, data = inp.read()
            if length:
                print('\nRecording. Press <space> to stop recording.\n')
                state = StreamState.RECORDING
        elif state == StreamState.RECORDING:
            # Non-blocking io to read from sound card
            length, data = inp.read()

            if length:
                data_np.append(data)
                #a = np.fromstring(data, dtype='int16')
                #data_np.extend(a.tolist())
        elif state == StreamState.PROCESSING:
            if len(data_np) == 0:
                state = StreamState.WAITING
                print('\nRecording interval too short. Press <space> to try recording again.\n')
                continue


            timestamp = time.strftime("%m.%d-%H.%M.%S")

            print('\nSaving recording as %s_%s.wav\n'%(filename, timestamp))
            wavfile = wave.open('%s_%s.wav'%(filename, timestamp),'w')
            wavfile.setparams((1, 2, sample_rate, 0, "NONE", "not compressed"))
            for chunk in data_np:
                wavfile.writeframes(chunk)
            wavfile.close()

            #Process sound for note detection
            bandpass_filter('%s_%s_bp.wav'%(filename, timestamp), input_name='%s_%s.wav'%(filename, timestamp))
            detect_notes('%s_%s.wav'%(filename, timestamp), sample_rate, True)

            state = StreamState.WAITING
            data_np = []
            print('\nFinished processing. Press <space> to record again.\n')

finally:
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)
