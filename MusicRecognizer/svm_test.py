import numpy
import pickle
from sklearn import svm
import json
import math

def saveSVM(clf):
	fileObject = open('svm', 'w')
	pickle.dump(clf, fileObject)
	fileObject.close()

def loadSVM():
		fileObject = open('svm', 'r')
		return pickle.load(fileObject)

def trainSVM():
		# read the training file
		print "Reading in training file..."
		#raw_data = numpy.genfromtxt('octaves_scales.wav_seg.csv', delimiter=",")
		raw_data = numpy.genfromtxt('scale_train.wav_seg.csv', delimiter=",")
		numFeatures = raw_data[0].size
		numData = raw_data.size / numFeatures

		X = raw_data[:, 0:numFeatures - 1]
		y = raw_data[:, numFeatures - 1]
		labels = numpy.copy(y)
		features = numpy.copy(X)
		clf = svm.SVC()
		print "Fitting the data"
		clf.fit(X, y)
		#saveSVM(clf)

		print "Predicting on training data"
		predictions = clf.predict(features)
		errors = 0
		print "Predictions: "
		for i in range(numData):
				if predictions[i] != labels[i]:
						errors += 1
						print "guessed: " + str(predictions[i]) + ", actual: " + str(labels[i])
		print "error rate: " + str(errors * 1.0 / numData)
		return clf

def estimate_fundamental(data):
	numEntries = data.size / data[0].size
	print 'Num Entries: ', numEntries
	result = []
	for i in range(0, numEntries):
		entry = data[i, :]
		fund = numpy.argmax(entry) + 1
		midi = 69 + 12 * math.log((fund * 1.0 / 440), 2)
		result.append(midi)
	print result

def run_test():
	clf = trainSVM()
	testdata = numpy.genfromtxt('./twinkle_short.wav_seg.csv', delimiter=",")
	print clf.predict(testdata)

def get_notes(testdata):
	clf = trainSVM()
	result = clf.predict(testdata)
	print result
	return result

if __name__ == '__main__':
		clf = trainSVM()
		# clf = loadSVM()
		testdata = numpy.genfromtxt('./twinkle_short.wav_seg.csv', delimiter=",")
		#estimate_fundamental(testdata)
		print clf.predict(testdata)
