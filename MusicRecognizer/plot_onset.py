
#! /usr/bin/env python

import sys
from aubio import onset, source
from numpy import hstack, zeros
import build_features, svm_test, redis

def detect_notes(filename, samplerate, plot=False):
    #r = redis.Redis(host='171.64.70.79');
    r = redis.Redis(host='localhost');

    #win_s = 512                 # fft size
    win_s = 4096                 # fft size
    hop_s = win_s / 2           # hop size

    s = source(filename, samplerate, hop_s)
    samplerate = s.samplerate
    o = onset("default", win_s, hop_s, samplerate)

    # list of onsets, in samples
    onsets = []
    onsets_seconds = []

    # storage for plotted data
    desc = []
    tdesc = []
    allsamples_max = zeros(0,)
    downsample = 1  # to plot n samples / hop_s

    #  print 'Threshold', o.get_threshold()
    o.set_threshold(0.5)
    #o.set_delay_s(0.3)

    # total number of frames read
    total_frames = 0
    while True:
        samples, read = s()
        if o(samples):
            print "%f" % (o.get_last_s())
            onsets.append(o.get_last())
            onsets_seconds.append(o.get_last_s())
        # keep some data to plot it later
        new_maxes = (abs(samples.reshape(hop_s/downsample, downsample))).max(axis=0)
        allsamples_max = hstack([allsamples_max, new_maxes])
        desc.append(o.get_descriptor())
        tdesc.append(o.get_thresholded_descriptor())
        total_frames += read
        if read < hop_s: break

    # now we have the offsets
    onsets.pop(0)
    onsets_seconds.pop(0)
    if len(onsets_seconds) == 0:
        print('\nNo sounds detected. Press <space> to try recording again.')
        return
    print onsets_seconds
    test_mat = build_features.extractFeaturesOnsets(filename, onsets)
    notes = svm_test.get_notes(test_mat)

    r.delete('music_notes')
    r.delete('music_times')
    #onsets_seconds = [1.356208,3.234542,5.026208,6.010417,6.504229,6.956167,7.887875,8.866542,10.721105,12.496708,16.049479,17.784605,19.532688,20.473770,20.945520,21.457146,22.353022,23.329979,24.269833,24.742313,25.211876,26.184416,27.142124]
    #notes = [ 60,  72,  71,  67,  69,  71,  72,  60,  69,  67,  57,  65,  64,  60,  62, 64,  65,  62,  59,  60,  62,  64,  60]

    #notes = [60, 60,  62, 60, 65, 64, 60, 60,  62, 60, 67, 65, 60, 60,   72, 69, 65, 64, 62, 70, 70,   69, 65, 67, 65]
    #onsets_seconds = [1,  1.5, 2,  3,  4,  5,  7,  8.5, 9,  10, 11, 12, 13, 14.5, 15, 16, 17, 18, 19, 21, 22.5, 23, 24, 25, 26]
    for i in range(0, len(notes)):
        r.rpush('music_notes', notes[i])
        r.rpush('music_times', onsets_seconds[i])

    #print dir(o)

    if plot:
	    # do plotting
	    import matplotlib.pyplot as plt
	    allsamples_max = (allsamples_max > 0) * allsamples_max
	    allsamples_max_times = [ float(t) * hop_s / downsample / samplerate for t in range(len(allsamples_max)) ]
	    plt1 = plt.axes([0.1, 0.75, 0.8, 0.19])
	    plt2 = plt.axes([0.1, 0.1, 0.8, 0.65], sharex = plt1)
	    plt.rc('lines',linewidth='.8')
	    plt1.plot(allsamples_max_times,  allsamples_max, '-b')
	    plt1.plot(allsamples_max_times, -allsamples_max, '-b')
	    for stamp in onsets:
	        stamp /= float(samplerate)
	        plt1.plot([stamp, stamp], [-1., 1.], '-r')
	    plt1.axis(xmin = 0., xmax = max(allsamples_max_times) )
	    plt1.xaxis.set_visible(False)
	    plt1.yaxis.set_visible(False)
	    desc_times = [ float(t) * hop_s / samplerate for t in range(len(desc)) ]
	    desc_plot = [d / max(desc) for d in desc]
	    plt2.plot(desc_times, desc_plot, '-g')
	    tdesc_plot = [d / max(desc) for d in tdesc]
	    for stamp in onsets:
	        stamp /= float(samplerate)
	        plt2.plot([stamp, stamp], [min(tdesc_plot), max(desc_plot)], '-r')
	    plt2.plot(desc_times, tdesc_plot, '-y')
	    plt2.axis(ymin = min(tdesc_plot), ymax = max(desc_plot))
	    plt.xlabel('time (s)')
	    #plt.savefig('/tmp/t.png', dpi=200)
	    plt.show()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Usage: %s <filename> [samplerate]" % sys.argv[0]
        sys.exit(1)

    filename = sys.argv[1]

    samplerate = 0
    if len( sys.argv ) > 2: samplerate = int(sys.argv[2])

    detect_notes(filename, samplerate, plot=True)
