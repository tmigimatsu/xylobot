import math
import numpy
import scipy.io.wavfile as wavfile
from scipy import signal
import sys

def stereoToMono(audiodata):
	if len(audiodata.shape) < 2:
		return audiodata
	d = audiodata.sum(axis=1) / 2
	return d

def readWav(filename):
	rate, data = wavfile.read(filename)
	return rate, data
