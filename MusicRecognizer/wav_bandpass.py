import numpy as np
import scipy.io.wavfile as wavfile
from scipy import signal
from scipy import fftpack
import matplotlib.pyplot as plt
import sys

def bandpass_filter(output_name, input_name=None, plot=False, data=None, rate=None):
    if input_name is not None:
        rate, data = wavfile.read(input_name)
    if len(data.shape) > 1:
        data = data[:,0]
    data = np.float64(data/32768.0) # Scale by 16-bits
    nyq = 0.5 * rate # Nyquist frequency
    pass_freq = [400 / nyq, 3600 / nyq] # pass between 400, 3600 Hz
    stop_freq = [100 / nyq, 4200 / nyq] # max attenuation below 100, above 4200 Hz
    pass_gain = 1.0 # tolerable loss in passband (dB)
    stop_gain = 20.0 # required attenuation in stopband (dB)
    N, Wn = signal.cheb2ord(pass_freq, stop_freq, pass_gain, stop_gain)
    b, a = signal.cheby2(N, stop_gain, Wn, btype = 'bandpass')
    filtered = signal.lfilter(b, a, data)
    filtered = np.minimum(filtered,1)
    wavfile.write(output_name, rate, np.int16(data*32768))

    if plot:
        ss = np.array(data)
        # Plot original audio
        plt.subplot(4,1,1)
        plt.plot(data)
        # Plot filtered audio
        plt.subplot(4,1,2)
        plt.plot(filtered)
        # Plot filter response
        plt.subplot(4,1,3)
        w, h = signal.freqz(b, a, worN=8000)
        plt.plot(nyq * w / np.pi, np.abs(h), 'b')
        plt.xlim(0, 6000)
        # Plot original audio fft
        plt.subplot(4,1,4)
        data_f = fftpack.rfft(data)
        plt.plot(np.abs(data_f))
        plt.show()

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print 'Usage: %s <input file> <output file>' % sys.argv[0]
        sys.exit(1)

    bandpass_filter(sys.argv[2], sys.argv[1], True)
