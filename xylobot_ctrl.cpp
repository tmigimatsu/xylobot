/* This file is part of scl, a control and simulation library
for robots and biomechanical models.

scl is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

Alternatively, you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

scl is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License and a copy of the GNU General Public License along with
scl. If not, see <http://www.gnu.org/licenses/>.
*/
/* \file task_main.cpp
 *
 *  Created on: Nov 22, 2010
 *
 *  Copyright (C) 2011
 *
 *  Author: Samir Menon <smenon@stanford.edu>
 */

#include "xylobot_ctrl.hpp"

using namespace scl_app;

//Set up the simulation function.
namespace scl_app {

bool XylobotCtrl::initMyController(int argc, char** argv) {
	try {
		// Parse the controllers
		for (const auto& task_name : task_names) {
			for (const auto& task : db->s_controller_.tasks_) {
				if (task->name_ == task_name) {
					tasks_ds[task_name] = dynamic_cast<scl::STaskOpPos *>(task);
					tasks_ds[task_name]->rbd_ = robot_gcm.rbdyn_tree_.at_const(tasks_ds[task_name]->link_name_);
					chai.addSphereToRender(robot_name, tasks_ds[task_name]->link_name_,
										   tasks_ds[task_name]->rbd_->T_o_lnk_ * tasks_ds[task_name]->pos_in_parent_);
					break;
				}
			}
		}

		// Iterate over all ground graphics
		std::list<chai3d::cGenericObject *> ground_graphics;
		auto it = chai_ds->robots_rendered_.at(0)->gr_tree_.end() - 1;
		it->graphics_obj_->enumerateChildren(ground_graphics);
		int i = 0;
		for (auto& graphics_obj : ground_graphics) {
			if (graphics_obj->m_userName != "scl_id_gr_box_ground") {
				// Skip non-cuboids
				i++;
				continue;
			}
			xylophone_keys.push_back(graphics_obj);

			// Get data structure containing parsed xml properties
			auto& xml_obj = robot_ds->rb_tree_.getRootNode()->graphics_obj_vec_[i];
			// std::cout << "Position: " << xml_obj.pos_in_parent_.transpose() << std::endl;
			// std::cout << "Orientation: " << xml_obj.ori_parent_quat_.transpose() << std::endl;
			// std::cout << "Scaling: " << xml_obj.scaling_.transpose() << std::endl;
			// std::cout << "Color: " << xml_obj.color_[0] << " " << xml_obj.color_[1] << " " << xml_obj.color_[2] << std::endl;
			// std::cout << std::endl;

			// get rotation matrix - default orientation of xylophone is along the x-axis,
			// rotate so that it's instead oriented along y-axis (90 degree counter-clockwise rotation)
			Eigen::Matrix3d tmp_rot_mat;
			tmp_rot_mat << 0, -1, 0,
			               1, 0, 0,
			               0, 0, 1;

			// get translation vector
			pos_xylophone << .52, 0, .34;
			pos_xylophone -= tasks_ds["hand_p"]->pos_in_parent_;

			// get transformed position vector
			pos_keys.push_back((tmp_rot_mat * xml_obj.pos_in_parent_) + pos_xylophone);

			// Set graphics position
			graphics_obj->setLocalPos(pos_keys.back()(0), pos_keys.back()(1), pos_keys.back()(2));

			// Set graphics orientation
			chai3d::cMatrix3d tmp_mat;
			tmp_mat(0,0) = tmp_rot_mat(0,0); tmp_mat(1,0) = tmp_rot_mat(1,0); tmp_mat(2,0) = tmp_rot_mat(2,0);
			tmp_mat(0,1) = tmp_rot_mat(0,1); tmp_mat(1,1) = tmp_rot_mat(1,1); tmp_mat(2,1) = tmp_rot_mat(2,1);
			tmp_mat(0,2) = tmp_rot_mat(0,2); tmp_mat(1,2) = tmp_rot_mat(1,2); tmp_mat(2,2) = tmp_rot_mat(2,2);
			graphics_obj->setLocalRot(tmp_mat);

			i++;
		}


		// Set robot to default position
		for (unsigned int i = 0; i < robot_io->dof_; i++) {
			robot_io->sensors_.q_(i) = robot_ds->rb_tree_.at(i)->joint_default_pos_;
		}
		dyn_scl.computeGCModel(&robot_io->sensors_, &robot_gcm);
		db->s_gui_.ui_point_[0] = tasks_ds["hand_p"]->rbd_->T_o_lnk_ * tasks_ds["hand_p"]->pos_in_parent_;

		// Create sphere at ui point
		chai.addSphereToRender(db->s_gui_.ui_point_[0], goal_sphere, 0.02);
		goal_sphere->m_material->m_ambient = chai3d::cColorf(255,0,0);

		// TODO: temporarily pushing scale to redis; delete me later
		if (redis.initialized) {
			// Chromatic scale
			// redisCommand(redis.context, "RPUSH %s 60 61 62 63 64 65 66 67 68 69 70 71 72", redis.key_notes);
			// redisCommand(redis.context, "RPUSH %s 0.0 2.0 4.0 6.0 8.0 10.0 12.0 14.0 16.0 18.0 20.0 22.0 24.0", redis.key_times);
			// Happy birthday
			//redisCommand(redis.context, "RPUSH %s 60 60 60 62 60 65 64 60 60 62 60 67 65 60 60 72 69 65 64 62 70 70 69 65 67 65", redis.key_notes);
			//redisCommand(redis.context, "RPUSH %s 0  2  3  4  6  8  10 14 15 16 18 20 22 26 27 30 32 34 36 38 42 43 44 46 48 50", redis.key_times);
			//redisCommand(redis.context, "RPUSH %s 60 60  62 60 65 64 60 60  62 60 67 65 60 60   72 69 65 64 62 70 70   69 65 67 65", redis.key_notes);
			//redisCommand(redis.context, "RPUSH %s 1  1.5 2  3  4  5  7  8.5 9  10 11 12 13 14.5 15 16 17 18 19 21 22.5 23 24 25 26", redis.key_times);
		}

		return true;

	} catch (std::exception &e) {
		std::cout << "XylobotCtrl::initMyController() : " << e.what() << std::endl;
	}

	return false;
}

void XylobotCtrl::communicateRedisMusic() {
	// Get xylophone notes
	redis.reply = (redisReply *) redisCommand(redis.context, "LPOP %s", redis.key_notes);
	while (redis.reply->type != REDIS_REPLY_NIL) {
		music_notes.push(std::atoi(redis.reply->str));
		redis.reply = (redisReply *) redisCommand(redis.context, "LPOP %s", redis.key_notes);
	}
	// Get xylophone times
	redis.reply = (redisReply *) redisCommand(redis.context, "LPOP %s", redis.key_times);
	while (redis.reply->type != REDIS_REPLY_NIL) {
		music_times.push(std::atof(redis.reply->str));
		redis.reply = (redisReply *) redisCommand(redis.context, "LPOP %s", redis.key_times);
	}
}

void XylobotCtrl::communicateRedisReceiveKinova() {
	if (!redis.initialized) return;

	try {
		// Get control mode
		if (music_status == XylophoneStatus::INITIALIZING) {
			redis.reply = (redisReply *) redisCommand(redis.context, "GET %s", redis.key_control_mode);
			if (redis.reply->type == REDIS_REPLY_NIL) return;
			control_mode = (KinovaControlMode) std::atoi(redis.reply->str);
		}

		// Do not get joint angles if in simulation mode
		if (control_mode == KinovaControlMode::SIMULATION) return;

		// Get joint angles
		float kinova_arr[KINOVA_DOF];
		float kinova_vel_arr[KINOVA_DOF];
		getRedisArray(redis.key_positions, kinova_arr);
		for (unsigned int i = 0; i < robot_io->dof_; i++) {
			if (isnan(kinova_arr[i])) throw std::runtime_error("NaN joint position");
		    robot_io->sensors_.q_(i) = kinova_arr[i];
		}
		if (control_mode == KinovaControlMode::STATIC) {
			std::cout << "q: " << robot_io->sensors_.q_.transpose()
			          << ", x: " << robot_gcm.rbdyn_tree_.at(tasks_ds["hand_p"]->link_name_)->T_o_lnk_.translation().transpose() << std::endl;
		}

		// Get joint velocities
		getRedisArray(redis.key_velocities, kinova_vel_arr);
		for (unsigned int i = 0; i < robot_io->dof_; i++) {
			if (isnan(kinova_vel_arr[i])) throw std::runtime_error("NaN joint velocity");
		    robot_io->sensors_.dq_(i) = kinova_vel_arr[i];
		}
	} catch (std::exception &e) {
		std::cout << "XylobotCtrl::redisCommunication() : " << e.what() << std::endl;
	}
}

void XylobotCtrl::communicateRedisSendKinova() {
	if (!redis.initialized) return;

	try {
		float kinova_arr[KINOVA_DOF];
		// Send control commands
		switch (control_mode) {

			// Torque control
			case KinovaControlMode::TORQUE:
				for (unsigned int i = 0; i < robot_io->dof_; i++) {
					kinova_arr[i] = Gamma_kinova(i);
				}
				setRedisArray(redis.key_torques, kinova_arr);

				if (iter_controller % 5000 == 0) {
					std::cout << "Torques: " << Gamma_kinova.transpose() << std::endl;
					std::cout << "Positions: " << robot_io->sensors_.q_.transpose() << std::endl;
				}
				break;

			// Position control
			case KinovaControlMode::POSITION:
				for (unsigned int i = 0; i < robot_io->dof_; i++) {
					kinova_arr[i] = robot_io->sensors_.q_(i);
				}
				setRedisArray(redis.key_positions, kinova_arr);

				if (iter_controller % 5000 == 0) {
					std::cout << "Positions: " << robot_io->sensors_.q_.transpose() << std::endl;
				}
				break;

			default:
				break;
		}
	} catch (std::exception &e) {
		std::cout << "XylobotCtrl::redisCommunication() : " << e.what() << std::endl;
	}
}

void XylobotCtrl::computeControlForces(enum ControlType control_type) {
	try {
		scl::SRigidBodyDyn *hand = robot_gcm.rbdyn_tree_.at(tasks_ds["hand_p"]->link_name_);

		// Compute Jacobians
		Eigen::MatrixXd J;
		// dyn_scl.computeJacobianWithTransforms(J, *hand, robot_io->sensors_.q_, tasks_ds["hand_p"]->pos_in_parent_);
		dyn_scl.computeJacobianWithTransforms(J, *hand, robot_io->sensors_.q_, Eigen::VectorXd::Zero(3));
		// dyn_scl.computeJacobianWithTransforms(J, *hand, robot_io->sensors_.q_, tasks_ds["hand_p"]->pos_in_parent_);
		Eigen::MatrixXd Jv = J.block(0, 0, 3, robot_io->dof_);
		Eigen::MatrixXd Jw = J.block(3, 0, 3, robot_io->dof_);

		// Get current position and velocity
		// Eigen::Vector3d x = hand->T_o_lnk_ * tasks_ds["hand_p"]->pos_in_parent_;
		Eigen::Vector3d x = hand->T_o_lnk_.translation();
		Eigen::Vector3d dx = J * robot_io->sensors_.dq_;
		x_err = x_des - x;
		dx_err = - dx;

		// Get current orientation and angular velocity
		Eigen::Matrix3d R = hand->T_o_lnk_.rotation();
		Eigen::Vector3d w = Jw * robot_io->sensors_.dq_;
		w_err = - w;

		// Calculate angular error vector
		d_phi << 0, 0, 0;
		for (int i = 0; i < 3; i++) {
			Eigen::Vector3d Ri = R.col(i);
			Eigen::Vector3d Rdi = R_des.col(i);
			d_phi += Ri.cross(Rdi);
		}
		d_phi *= -0.5;

		// Operational space Inertia matrix
		Eigen::MatrixXd Lambda_x = (Jv * robot_gcm.M_gc_inv_ * Jv.transpose()).inverse();
		Eigen::MatrixXd Lambda_r = (Jw * robot_gcm.M_gc_inv_ * Jw.transpose()).inverse();

		// Nullspace projection
		Eigen::MatrixXd Jw_bar = robot_gcm.M_gc_inv_ * Jw.transpose() * Lambda_r;
		Eigen::MatrixXd Jv_bar = robot_gcm.M_gc_inv_ * Jv.transpose() * Lambda_x;
		Eigen::MatrixXd N_r = Eigen::MatrixXd::Identity(robot_io->dof_ , robot_io->dof_) - Jw_bar * Jw;
		Eigen::MatrixXd N_x = Eigen::MatrixXd::Identity(robot_io->dof_ , robot_io->dof_) - Jv_bar * Jv;
		Eigen::MatrixXd Jv_N = Jv * N_r;
		Eigen::MatrixXd Jw_N = Jw * N_x;
		Eigen::MatrixXd Lambda_x_N = (Jv_N * robot_gcm.M_gc_inv_ * Jv_N.transpose()).inverse();
		Eigen::MatrixXd Lambda_r_N = (Jw_N * robot_gcm.M_gc_inv_ * Jw_N.transpose()).inverse();

		// Get task gains and max forces from specs
		double kp_x = tasks_ds["hand_p"]->kp_(0), kv_x = tasks_ds["hand_p"]->kv_(0);
		double kp_r = tasks_ds["hand_r"]->kp_(0), kv_r = tasks_ds["hand_r"]->kv_(0);
		double ddx_max = tasks_ds["hand_p"]->force_task_max_(0);
		double dw_max = tasks_ds["hand_r"]->force_task_max_(0);

		// Saturate position spring force
		Eigen::Vector3d ddx = kp_x * x_err;
		Eigen::Vector3d dw = kp_r * -d_phi;
		if (ddx.norm() > ddx_max) ddx = ddx/ddx.norm() * ddx_max;
		if (dw.norm() > dw_max) dw = dw/dw.norm() * dw_max;

		// Add velocity damping
		ddx += kv_x * dx_err;
		dw += kv_r * w_err;

		// Compute spring force with damping
		Eigen::VectorXd F_x = Lambda_x * ddx;
		Eigen::VectorXd F_x_N = Lambda_x_N * ddx;
		Eigen::VectorXd F_r = Lambda_r * dw;
		Eigen::VectorXd F_r_N = Lambda_r_N * dw;

		// Compute component joint torques
		Eigen::VectorXd Gamma_x = Jv.transpose() * F_x;
		Eigen::VectorXd Gamma_x_N = Jv_N.transpose() * F_x_N;
		Eigen::VectorXd Gamma_r = Jw.transpose() * F_r;
		Eigen::VectorXd Gamma_r_N = Jw_N.transpose() * F_r_N;

		// Compute full Lambda matrix
		Eigen::MatrixXd Lambda = (J * robot_gcm.M_gc_inv_ * J.transpose()).inverse();
		Eigen::Vector6d ddx_dw;
		ddx_dw << ddx, dw;
		Eigen::VectorXd F = Lambda * ddx_dw;

		// Joint torques
		// Gamma = Gamma_x + Gamma_r;
		// Gamma = Gamma_x + Gamma_r_N;
		// Gamma = J.transpose() * F;
		// Gamma = Gamma_x;
		Gamma = Gamma_r;
		Eigen::VectorXd Gamma_N;
		switch (control_type) {
			case EQUAL_PRIORITY:
				Gamma = J.transpose() * F;
				Gamma_N = Eigen::Vector6d::Zero();
				break;

			case POSITION_PRIORITY:
				Gamma = Gamma_x;
				Gamma_N = Gamma_r_N;
				break;

			case ORIENTATION_PRIORITY:
				Gamma = Gamma_r;
				Gamma_N = Gamma_x_N;
				break;

			default:
				throw std::runtime_error("Not a valid control type");
				break;
		}

		// Enforce joint limits
		// robot_ds->rb_tree_.at(i)->joint_limit_lower_
		// -1 / ((q-q_max)(q-q_max+2q_margin)) -> (q-q_max+q_margin)/((q-q_max)(q-q_max+2q_margin))^2
		// -1 / ((q-q_min)(q-q_min-2q_margin)) -> (q-q_min-q_margin)/((q-q_min)(q-q_min-2q_margin))^2
		// double q_margin = 0.2;
		for (unsigned int i = 0; i < robot_io->dof_; i++) {
			double q = robot_io->sensors_.q_(i);
			double q_min = robot_ds->rb_tree_.at(i)->joint_limit_lower_;
			double q_max = robot_ds->rb_tree_.at(i)->joint_limit_upper_;
			if (q < q_min || q > q_max) {
				// std::cout << "Warning: q_" << i << " (" << q << ") beyond joint limits (" << q_min << ", " << q_max << ")" << std::endl;
			}

			/*
			if (q < q_min) {
				Gamma(i) += 100;
			} else if (q < q_min + q_margin) {
				double denom = (q - q_min) * (q - q_min - 2*q_margin);
				Gamma(i) += robot_gcm.M_gc_(i,i) * (q - q_min - q_margin) / (denom * denom);
				if (iter_controller % 1000 == 0)
				std::cout << "q_" << i << ": " << q << ", q_min:" << q_min << ", ddq:" << (q-q_min-q_margin)/(denom*denom) << ", A: " << robot_gcm.M_gc_(i,i) << std::endl;
			} else if (q > q_max) {
				Gamma(i) -= 100;
			} else if (q > q_max - q_margin) {
				double denom = (q - q_max) * (q - q_max + 2*q_margin);
				Gamma(i) += robot_gcm.M_gc_(i,i) * (q - q_max + q_margin) / (denom * denom);
				if (iter_controller % 1000 == 0)
				std::cout << "q_" << i << ": " << q << ", q_max:" << q_max << ", ddq:" << (q-q_max+q_margin)/(denom*denom) << ", A: " << robot_gcm.M_gc_(i,i) << std::endl;
			}
			*/
		}

		double torque_margin = 1;
		for (unsigned int i = 0; i < robot_io->dof_; i++) {
			// Enforce torque limits
			if (Gamma(i) > robot_ds->rb_tree_.at(i)->force_gc_lim_upper_) {
				Gamma(i) = robot_ds->rb_tree_.at(i)->force_gc_lim_upper_;
			} else if (Gamma(i) < robot_ds->rb_tree_.at(i)->force_gc_lim_lower_) {
				Gamma(i) = robot_ds->rb_tree_.at(i)->force_gc_lim_lower_;
			}

			// In the remaining torque margin, add the orientation torques
			if (Gamma_N(i) == 0) continue;
			if ((robot_ds->rb_tree_.at(i)->force_gc_lim_upper_ - Gamma(i))/abs(Gamma_N(i)) < torque_margin) {
				torque_margin = (robot_ds->rb_tree_.at(i)->force_gc_lim_upper_ - Gamma(i))/abs(Gamma_N(i));
			}
			if ((Gamma(i) - robot_ds->rb_tree_.at(i)->force_gc_lim_lower_)/abs(Gamma_N(i)) < torque_margin) {
				torque_margin = (Gamma(i) - robot_ds->rb_tree_.at(i)->force_gc_lim_lower_)/abs(Gamma_N(i));
			}
		}
		if (isinf(torque_margin) || isnan(torque_margin)) throw std::runtime_error("torque_margin is NaN or infinite");
		Gamma += torque_margin * Gamma_N;

		if (iter_controller % 10000 == 0) {
			std::cout << "Gamma: " << Gamma.transpose() << std::endl;
		}

		Gamma_kinova = Gamma;
		Gamma -= robot_gcm.force_gc_grav_;

		for (unsigned int i = 0; i < robot_io->dof_; i++) {
			if (isnan(Gamma(i))) {
				std::cout << std::endl << "Jv : " << std::endl << Jv << std::endl;
				std::cout << "F_x : " << F_x.transpose() << std::endl;
				std::cout << "Jw : " << std::endl << Jw << std::endl;
				std::cout << "F_r : " << F_r.transpose() << std::endl;
				throw std::runtime_error("Nan torques");
			}
		}

		robot_io->actuators_.force_gc_commanded_ = Gamma;

	} catch (std::exception &e) {
		std::cout << "XylobotCtrl::computeControlForces() : " << e.what() << std::endl;
		Gamma << 0, 0, 0, 0, 0, 0;
		terminate(1);
	}
}

bool XylobotCtrl::endEffectorIsStable() {
	std::cout << "x_err: " << x_err.norm() << " dx_err: " << dx_err.norm() << " d_phi: " << d_phi.norm() << " w_err: " << w_err.norm() << std::endl;
	return x_err.norm()  <= 0.02
	    && dx_err.norm() <= 0.01
	    && d_phi.norm()  <= 0.05
	    && w_err.norm()  <= 0.01;
}

void XylobotCtrl::stepMySimulation() {
	try {
		// Compute dynamic model
		communicateRedisReceiveKinova();
		communicateRedisMusic();
		dyn_scl.computeGCModel(&robot_io->sensors_, &robot_gcm);

		// Compute control torques
		if (music_status == XylophoneStatus::INITIALIZING) {

			// Desired orientation
			R_des = Eigen::Matrix3d::Identity();
			R_des(0, 0) = -1;
			R_des(2, 2) = -1;

			// Desired position
			x_des = db->s_gui_.ui_point_[0];
			x_des = pos_xylophone;
			x_des(2) += 0.03;
			goal_sphere->setLocalPos(x_des(0), x_des(1), x_des(2));

			Eigen::Vector3d ee_offset = - R_des * tasks_ds["hand_p"]->pos_in_parent_;
			x_des += ee_offset;

			computeControlForces(POSITION_PRIORITY);
			// computeControlForces(ORIENTATION_PRIORITY);

			// TODO: Make d_phi error margin stricter
			if (endEffectorIsStable()) {
				if (!music_notes.empty()) {
					music_status = XylophoneStatus::QUIET;
					t_goal = music_times.front();
					music_times.pop();

					note_goal = music_notes.front();
					music_notes.pop();
				}
			}

		} else if (music_status == XylophoneStatus::QUIET) {
			Eigen::Vector3d ee_offset = - R_des * tasks_ds["hand_p"]->pos_in_parent_;

			// Initialize to first note
			int pos_key_idx = getXylophoneKeyIdx(note_goal);
			if (pos_key_idx == -1)
				throw std::runtime_error("Invalid note detected: " + std::to_string(note_goal));
			x_des = pos_keys[pos_key_idx];
			goal_sphere->setLocalPos(x_des(0), x_des(1), x_des(2));

			x_des += ee_offset;
			if (endEffectorIsStable()) {
				// Drop the end effector now!
				t_start = t_curr - (t_goal - time_mallet);
				music_status = XylophoneStatus::PLAYING;
			}
			computeControlForces(ORIENTATION_PRIORITY);

			// Get the next note when t_curr has reached t_goal
			// TODO: Play note only if end effector has reached goal, and also is past the required time
			// TODO: Fix bug where first note isn't played
			// TODO: Calculate minimum time interval based on spacing between notes, maybe in python?

		} else if(music_status == XylophoneStatus::PLAYING) {

			if (t_curr - t_start >= t_goal - time_mallet && endEffectorIsStable()) {

				// Drop mallet
				if (fd_arduino >= 0) {
					uint8_t byte = '1';
					if (write(fd_arduino, &byte, 1) < 0)
						std::cout << "could not write 1 to arduino" << std::endl;
				}
				t_wait = t_curr - t_start + time_mallet;

				// Restore previous key color
				if (note_goal_prev != 0) {
					xylophone_keys[getXylophoneKeyIdx(note_goal_prev)]->m_material->setColor(color_key);
				}

				// Set hit key color
				// TODO: define key width, get error at mallet position
				// TODO: keep cColorf instead of double array
				const double *color_tone = x_err.norm() <= 0.1 ? color_key_pressed : color_key_missed;
				color_key = xylophone_keys[getXylophoneKeyIdx(note_goal)]->m_material->m_diffuse;
				chai3d::cColorf tmp_col(color_tone[0], color_tone[1], color_tone[2]);
				xylophone_keys[getXylophoneKeyIdx(note_goal)]->m_material->setColor(tmp_col);

				music_status = XylophoneStatus::STRIKING;
			}

			computeControlForces(ORIENTATION_PRIORITY);

		} else {

			// Wait for mallet to return
			if (t_curr - t_start >= t_wait) {

				// Return if no notes left
				if (music_notes.empty()) {
					if (note_goal != 0) {
						xylophone_keys[getXylophoneKeyIdx(note_goal)]->m_material->setColor(color_key);
					}
					music_status = XylophoneStatus::INITIALIZING;
					t_goal = 0;
					t_goal_interval = 0;
					note_goal = 0;
					note_goal_prev = 0;
					return;
				}

				// Get the next note
				// t_goal = music_times.front();
				t_goal_interval = music_times.front() - t_goal;
				t_goal = t_curr - t_start + t_goal_interval;
				music_times.pop();

				note_goal_prev = note_goal;
				note_goal = music_notes.front();
				music_notes.pop();

				// Desired position
				int pos_key_idx = getXylophoneKeyIdx(note_goal);
				if (pos_key_idx == -1)
					throw std::runtime_error("Invalid note detected: " + std::to_string(note_goal));
				x_des = pos_keys[pos_key_idx];
				goal_sphere->setLocalPos(x_des(0), x_des(1), x_des(2));

				Eigen::Vector3d ee_offset = - R_des * tasks_ds["hand_p"]->pos_in_parent_;
				x_des += ee_offset;

				music_status = XylophoneStatus::PLAYING;

			}

			computeControlForces(ORIENTATION_PRIORITY);

		}

		// Send command to robot
		communicateRedisSendKinova();

		// Integrate the dynamics
		if (control_mode == SIMULATION || control_mode == POSITION) {
			dyn_tao.integrate(robot_gcm, *robot_io, db->sim_dt_);
		}

	} catch (std::exception &e) {
		std::cout << "XylobotCtrl::stepMySimulation() : " << e.what() << std::endl;
	}

}

}

/** A sample application to demonstrate marker tracking with
 * an operational space controller on a robot. */
int main(int argc, char** argv) {
	// Install Signal Handler
	// signal(SIGINT, signalHandler);

	if (argc >= 2 && strcmp(argv[1], "-h") == 0) {
		std::cout << std::endl << "The command line input is: ./<executable> <specs_file.xml> <robot_name> <controller_name> <operational task point>" << std::endl << std::endl;
		return 0;
	}

	XylobotCtrl app;

	app.init(argc,argv);

	/***********************Main Loop*****************************/
	double t_sim_start = sutil::CSystemClock::getSysTime();

#ifndef DEBUG
	app.runMainLoopThreaded();  //Run multi-threaded in release mode
#else
	app.runMainLoop();          //Run single-threaded in debug mode
#endif

	double t_sim_end = sutil::CSystemClock::getSysTime();
	std::cout << "Simulation Took Time : " << t_sim_end - t_sim_start << " sec" << std::endl;

	/****************************Deallocate Memory And Exit*****************************/
	app.terminate(0);
	return 0;
}
